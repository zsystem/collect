<?php
// checking for minimum PHP version
include("include/classes/session.php");
include("include/connection.php");
if (($session->logged_in) == true) {

?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Contract - Collect+</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="css/googleapis.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="views/reset.css" type="text/css">
    <link rel="stylesheet" href="views/style.css" type="text/css">
    <link rel="stylesheet" href="views/zebra_pagination.css" type="text/css">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 <style type="text/css">
<!--
.a {	color: #FFF;
} 
 body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
}

	
-->
  </style>
  <style type="text/css">

  </style>
<script src="sorttable.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">



<script type="text/javascript" src="x.js"></script> 
<script type="text/javascript"> 
</script>
  </head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		
		    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a>     <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> </div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
      		            <li><a href="zone.php">Zone</a></li>
            <li><a href="cycle.php">Cycle & Period Code</a></li>
            <li><a href="limit.php">Customer Limit</a></li>
            <li><a href="status.php">Status</a></li>
            <li><a href="severity.php">Feedback Severity</a></li>
			<li class="active"><a href="#">Feedback Status</a></li>


          </ul>
          <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li class="active"><a href="#"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?> <?php if ( ($session->isMaster()) || ($session->isAgent())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>	
		 <li><a href="workcard.php"><i class=" icon-edit"></i><span>Work Card</span></a></li><li  class="active"><a href="#"><i class="icon-th-large"></i><span>Contract</span> </a></li>
        <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pushpin"></i>
	      				<h3>Customer Contract</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
                        <li ><a href="contract.php">Inquiry</a></li>
						  <li class="active">
						    <a href="#formcontrols" data-toggle="tab">Maintenance</a>
						  </li>

						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane" id="formcontrols">

								</div>
								
								<div class="tab-pane active" id="jscontrols">
                              <div id="collapseOne" class="accordion-body collapse in">
                                                                                
                                                          <div class="accordion-inner">
<input type="text" id="search" placeholder="Type to search">
                                                          </div></div>
                                                          

 <form action="con_process.php" method="post" enctype="multipart/form-data">
 <div class="form-actions">
<script type="text/javascript">
	$(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton0').attr('disabled', 'disabled');
                }
            else {
                $('#submitButton0').removeAttr('disabled');
                }
            });
        });										
    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton1').removeAttr('disabled');
                }
            else {
                $('#submitButton1').attr('disabled', 'disabled');
                }
            });
        });
	    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton2').removeAttr('disabled');
                }
            else {
                $('#submitButton2').attr('disabled', 'disabled');
                }
            });
        });
	    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton3').attr('disabled', 'disabled');
                }
            else {

			 	$('#submitButton3').removeAttr('disabled');
                }
            });
        });	
</script> 
   <input name="button" type="submit" class="btn btn-small" value="Add   ">    
  <input name="button" type="submit" class="btn btn-small" value="Edit  " disabled id="submitButton1"> 
   <!-- Button to trigger modal--> 
 <!--<a href="#myModal" role="button" class="btn-warning" data-toggle="modal"><input name="button" type="submit" class=" btn btn-warning" value="Delete" disabled id="submitButton2"></a>-->
 <a href="con_edit.php" role="button" class="btn btn-small">Reset</a>
  <!-- Modal -->
                                                    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h3 id="myModalLabel">WARNING</h3>
                                                      </div>
                                                      <div class="modal-body">
                                                        <p>Deleted record cannot be recovered anymore. Do you wish to proceed?</p>
                                                      </div>
                                                      <div class="modal-footer">
                                                         <input name="button" class="btn btn-primary"type="submit" class="btn btn-small" value="OK">
                                                        <button class="btn btn-small" data-dismiss="modal" aria-hidden="true">NO</button>
                                                        
                                                      </div>
                                                    </div>
										</div>
 <div class="control-group">											
<?php

        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

        // how many records should be displayed on a page?
        $records_per_page = 20;

        // include the pagination class
        require 'Zebra_Pagination.php';

        // instantiate the pagination object
        $pagination = new Zebra_Pagination();

        // set position of the next/previous page links
        $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

        // the MySQL statement to fetch the rows
        // note how we build the LIMIT
        // also, note the "SQL_CALC_FOUND_ROWS"
        // this is to get the number of rows that would've been returned if there was no LIMIT
        // see http://dev.mysql.com/doc/refman/5.0/en/information-functions.html#function_found-rows
        $MySQL = '
            SELECT
                SQL_CALC_FOUND_ROWS
               a.contract_id, a.property_id, b.name, b.company_name, c.status_desc, a.start_date, a.end_date, a.accno
            FROM
                contract a JOIN cfmast b ON b.id = a.customer_id
				LEFT JOIN statmast c on c.id = a.status
            ORDER BY
                contract_id
            LIMIT
                ' . (($pagination->get_page() - 1) * $records_per_page) . ', ' . $records_per_page . '
        ';


        // if query could not be executed
        if (!($result = @mysql_query($MySQL)))

            // stop execution and display error message
            die(mysql_error());

        // fetch the total number of records in the table
        $rows = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows'));

        // pass the total number of records to the pagination class
        $pagination->records($rows['rows']);

        // records per page
        $pagination->records_per_page($records_per_page);

        ?>

 <form name="myForm">
<table border="1" class="table" name="myselect" id="table">
    <thead>
        	<tr>
            <th width="2%" bgcolor="#333" class="widget-table">Select</th>
            <th width="10%" bgcolor="#333">Company Name</th>
            <th width="10%" bgcolor="#333">Contact Person</th>
            <th width="10%" bgcolor="#333">Account Number</th>
 			<th width="3%" bgcolor="#333">Status</th>
            <th width="10%" bgcolor="#333">Start Date</th>
            <th width="10%" bgcolor="#333">End Date</th>
            </tr>
	 </thead>
            <?php $index = 0?>

            <?php while ($row = mysql_fetch_assoc($result)):?>
<tbody>
            <tr>
                <td><input name="id" type="radio" value="<?php echo $row["contract_id"];?>"/></td>
                <td><?php echo $row['company_name']?></a></td>
                 <td><?php echo $row['name']?></a></td>
                 <td><?php echo $row['accno']?></a></td>
    <?php if ($row['status_desc'] =='ACTIVE') {?><td bgcolor="#00CC66"><font color="#000000"><?php echo $row['status_desc']; ?></font></td>
    <?php }else if ($row['status_desc'] =='BLOCKED') {?><td bgcolor="#FFCC00"><font color="#000000"><?php echo $row['status_desc'];?></font></td>
    <?php }else if ($row['status_desc'] =='EXPIRED') {?><td bgcolor="#FF0000"><font color="#000000"><?php echo $row['status_desc'];?></font></td>
    <?php }else if ($row['status_desc'] =='RENEWED') {?><td bgcolor="#339900"><font color="#000000"><?php echo $row['status_desc'];}?></font></td>
                  <td><?php echo $row['start_date']?></a></td>
                   <td><?php echo $row['end_date']?></a></td>
            </tr>
</tbody>

            <?php endwhile?>
        </table>

        <script type="text/javascript" src="jquery-1.7.2.js"></script>
          <script type="text/javascript" src="views/javascript/zebra_pagination.js"></script>
	      			
  <div class="accordion-inner">
	          <?php

        // render the pagination links
        $pagination->render();

        ?>
 	</div>	
    <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>									
<div class="controls">
</div>	<!-- /controls -->			
</div> <!-- /control-group -->

                                        
										
											
										 <br />
										
											
										<div class="form-actions">
 
<input name="button" type="submit" class="btn btn-small" value="Add   ">    
<input name="button" type="submit" class="btn btn-small" value="Edit  " disabled id="submitButton2"> 
<a href="con_edit.php" role="button" class="btn btn-small">Reset</a>
  <!-- Modal -->
                                                    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h3 id="myModalLabel">WARNING</h3>
                                                      </div>
                                                      <div class="modal-body">
                                                        <p>Deleted record cannot be recovered anymore. Do you wish to proceed?</p>
                                                      </div>
                                                      <div class="modal-footer">
                                                         <input name="button" class="btn btn-primary"type="submit" class="btn btn-small" value="OK">
                                                        <button class="btn btn-small" data-dismiss="modal" aria-hidden="true">NO</button>
                                                        
                                                      </div>
                                                    </div>
										</div> <!-- /form-actions -->
									</fieldset>
							  </form>
                          </div>
                                                        </div>	
      		

								</div>
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
var $rows = $('#table tbody tr');
$('#search').keyup(function () {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function () {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});
</script>

  </body>

</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>