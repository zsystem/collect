<?php

include("header.php");
include("include/classes/session.php");
include("include/connection.php");
include("script_gb.php");



if (($session->logged_in) == true) {

?>

<body>


<?php include("menubar.php"); ?>
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<span class="icon-pushpin"></span>
							<h3>Template Page 1</h3>
          </div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
 <!--	Business Logic to code here  -->
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<?php include('footer.php'); ?>
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    

  </body>

<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>