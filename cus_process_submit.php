<?php
// checking for minimum PHP version
include("include/classes/session.php");
include("include/connection.php");
if (($session->logged_in) == true) {


?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Customer - Collect+</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="css/googleapis.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
 
  </head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		
		    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a>     <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> </div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
      		<li><a href="zone.php">Zone</a></li>
            <li><a href="cycle.php">Cycle & Period Code</a></li>
            <li><a href="limit.php">Customer Limit</a></li>
            <li><a href="status.php">Status</a></li>
            <li><a href="severity.php">Feedback Severity</a></li>
			<li><a href="feedback.php">Feedback Status</a></li>
          </ul>
          <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li class="active"><a href="#"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?> <?php if ( ($session->isMaster()) || ($session->isAgent())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>	
		 <li><a href="workcard.php"><i class=" icon-edit"></i><span>Work Card</span></a></li><li><a href="contract.php"><i class="icon-th-large"></i><span>Contract</span> </a></li>
        <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li class="active"><a href="#"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pushpin"></i>
	      				<h3>Customer Profile</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
                        <li ><a href="customer.php">Inquiry</a></li>
						  <li class="active">
						    <a href="#formcontrols" data-toggle="tab">Maintenance</a>
						  </li>

						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane" id="formcontrols">

								</div>
								
								<div class="tab-pane active" id="jscontrols">

										

	      			
 
	          <?php
/**
 * Process.php
 * 
 * The Process class is meant to simplify the task of processing
 * user submitted forms, redirecting the user to the correct
 * pages if errors are found, or if form is successful, either
 * way. Also handles the logout procedure.
 */


$mode = $_REQUEST['button'];


if(isset($_POST['button'])) {
if (empty($mode)) {
echo 'invalid';
}//close if empty mode
else if ($mode = $_REQUEST['button']) { 
switch ($mode) {
    case "Create   ": 
?>
<h5>Maintenance: Add</h5>
 <div class="accordion-inner">
<?php
 
        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // grab values
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');
$image_types = Array ("image/bmp", 
                        "image/jpeg", 
                        "image/pjpeg", 
                        "image/gif", 
                        "image/x-png"); 			
$userfile  = addslashes (fread 
 (fopen ($_FILES["userfile"]["tmp_name"], "r"), 
 filesize ($_FILES["userfile"]["tmp_name"]))); 
$file_name = $_FILES["userfile"]["name"]; 
$file_size = $_FILES["userfile"]["size"]; 
$file_type = $_FILES["userfile"]["type"]; 
$salut = $_REQUEST['salut'];
$name = $_REQUEST['name'];
$mail = $_REQUEST['mail'];
$post = $_REQUEST['post'];
$idtype = $_REQUEST['idtype'];
$idnum = $_REQUEST['idnum'];
$office = $_REQUEST['office'];
$mobile = $_REQUEST['mobile'];
$company_name = $_REQUEST['company'];
$lot = $_REQUEST['lot'];
$add1 = $_REQUEST['add1'];
$add2 = $_REQUEST['add2'];
$add3 = $_REQUEST['add3'];
$postcode = $_REQUEST['postcode'];
$area = $_REQUEST['sub_menu_id'];
$zone = $_REQUEST['zone'];
$lmt = $_REQUEST['lmt'];
$date =  date('Y-m-d H:i:s');
$user=$session->username;

$q = "SELECT * FROM `postcode` where postcode = ".$postcode;
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($row=mysql_fetch_array($results)) 
{
date_default_timezone_set("Asia/Kuala_Lumpur");
$date =  date('Y-m-d H:i:s'); //Returns IST 	
$state =  $row['state_code'];	

}

$q = "SELECT * FROM `cfmast` where company_name = '".$company_name."'";
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());

if (empty($row)){
/*$q = "SELECT * FROM `propmast` where address_1 = ".$address1;
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
$rows=mysql_num_rows($results);
if (empty($rows)) {
//$r=$rows["address_1"];*/	
$sql = "INSERT INTO cfmast (`id`, `company_name`, `zone_id`, `salutation`, `name`, `user_email`, `post`, `office_no`, `mobile_no`, `id_type`, `id_no`, `lot`, `address_1`, `address_2`, `address_3`, `postcode`, `area`, `state_code`, `status_id`, `limit_id`, `image`, `image_type`, `image_size`, `date_added`,`user`) VALUES ('', '$company_name', '$zone', '$salut', '$name', '$mail', '$post ', '$office', '$mobile', '$idtype', '$idnum','$lot','$add1','$add2','$add3','$postcode','$area', '$state', '1',  '$lmt', '$userfile', '$file_type', '$file_size', '$date', '$user' )";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());

		   if ($ok)
		   {
		   echo "Record Saved";
		   }
		   else 
		   {
		   echo "<font color = 'red' size = '3'>WARNING:</font> Record Existed";
		   break;
		   }}
unset($_POST);
header("Location:cus_edit.php");



?>
 </div>
<?php

	 break;
     case "Save   ":
?>
<h5>Maintenance: Edit</h5>
   <div class="accordion-inner">
<?php
require_once("include/connection.php");
 
        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

$id = $_REQUEST['id'];

$salut = $_REQUEST['salut'];
$name = $_REQUEST['name'];
$mail = $_REQUEST['mail'];
$post = $_REQUEST['post'];
$idtype = $_REQUEST['idtype'];
$idnum = $_REQUEST['idnum'];
$office = $_REQUEST['office'];
$lmt = $_REQUEST['lmt'];
$mobile = $_REQUEST['mobile'];
$company_name = $_REQUEST['company'];
$lot = $_REQUEST['lot'];
$add1 = $_REQUEST['add1'];
$add2 = $_REQUEST['add2'];
$add3 = $_REQUEST['add3'];
$postcode = $_REQUEST['postcode'];
$area = $_REQUEST['sub_menu_id'];
$zone = $_REQUEST['zone'];
$date =  date('Y-m-d H:i:s');
$user=$session->username;

date_default_timezone_set("Asia/Kuala_Lumpur");
$date =  date('Y-m-d H:i:s'); //Returns IST 
$user=$session->username;

$q = "SELECT * FROM `postcode` where postcode = ".$postcode;//populate state_code
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($row=mysql_fetch_array($results)) 
{
date_default_timezone_set("Asia/Kuala_Lumpur");
	
$state =  $row['state_code'];	
}

$q = "SELECT * FROM `cfmast` where id = ".$id;//check changes
$results = mysql_query ($q) or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($r = mysql_fetch_array($results)) {
//fetching record
$osalut = $r['salutation'];
$oname = $r['name'];
$omail = $r['user_email'];
$opost = $r['post'];
$oidtype = $r['id_type'];
$oidnum = $r['id_no'];
$ooffice = $r['office_no'];
$omobile = $r['mobile_no'];
$ocompany_name = $r['company_name'];
$olot = $r['lot'];
$oadd1 = $r['address_1'];
$oadd2 = $r['address_2'];
$oadd3 = $r['address_3'];
$opostcode = $r['postcode'];
$oarea = $r['area'];
$ozone = $r['zone_id'];
$olmt = $r['limit_id'];

$date =  date('Y-m-d H:i:s'); //Returns IST 
//dumping history	
		   if ($osalut <> $salut)//check salutation
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','SALUTATION', 'UPDATE', '$osalut', '$salut', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}
		   if ($ocompany_name <> $company_name)//check name
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','COMPANY NAME', 'UPDATE', '$ocompany_name', '$company_name', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}
		   if ($omail <> $mail)//check email
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','E-MAIL', 'UPDATE', '$omail', '$mail', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}
		   if ($opost <> $post)//check designation
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','DESIGNATION', 'UPDATE', '$opost', '$post', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}
		   if ($oidtype <> $idtype)//check id type
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','ID TYPE', 'UPDATE', '$idt', '$oidtype', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}		
		   if ($oidnum <> $idnum)//check id num
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','ID NUM', 'UPDATE', '$oidnum', '$idnum', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   if ($ooffice <> $office)//check office num
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','OFFICE NUM', 'UPDATE', '$ooffice', '$office', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}
		   if ($omobile <> $mobile)//check mobile num
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','MOBILE NUM', 'UPDATE', '$omobile', '$mobile', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
 		   else {}
		   if ($olot <> $lot)//check lot
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','LOT NO', 'UPDATE', '$olot', '$lot', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}	
		   if ($oadd1 <> $add1)//check address1
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','ADDRESS 1', 'UPDATE', '$oadd1', '$add1', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}
		   if ($oadd2 <> $add2)//check address2
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','ADDRESS 2', 'UPDATE', '$oadd2', '$add2', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}	
		   if ($oadd3 <> $add3)//check address3
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','ADDRESS 3', 'UPDATE', '$oadd3', '$add3', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}
		   if ($opostcode <> $postcode)//check postcode
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','POSTCODE', 'UPDATE', '$opostcode', '$postcode', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}		   
		   if ($oarea <> $area)//check area
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','AREA', 'UPDATE', '$oarea', '$area', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}
		   if ($ozone <> $zone)//check zone
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','ZONE', 'UPDATE', '$ozone', '$zone', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}	
		   if ($olmt <> $lmt)//check limit
		   {
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','LIMIT', 'UPDATE', '$olmt', '$lmt', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());   
		   }
		   else {}		   
//update cfmast		   
$sqls = "UPDATE cfmast set company_name= '$company_name', zone_id = '$zone', salutation ='$salut', name ='$name', user_email = '$mail', post = '$post', office_no = '$office', mobile_no = '$mobile', id_type = '$idtype', id_no = '$idnum', lot = '$lot', address_1 = '$add1', address_2 = '$add2', address_3 = '$add3', postcode = '$postcode', area = '$area', state_code = '$state', limit_id = '$lmt' where id = ".$id;
$ok1=mysql_query($sqls) or die("ERROR GETTING DATA. REASON: " . mysql_error());
		   if ($ok1)
		   {
		   echo "Record Saved";
		   }
		   else 
		   {
		   echo "<font color = 'red' size = '3'>WARNING:</font> Unable to update. Please contact system administrator";
		   break;
		   }
}
unset($_POST);
header("Location:cus_edit.php");
?>
 </div>
<?php
	 break;
    case "OK":

$id = "";
$id = $_REQUEST['id'];
       // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // grab values
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');
$image_types = Array ("image/bmp", 
                        "image/jpeg", 
                        "image/pjpeg", 
                        "image/gif", 
                        "image/x-png"); 			
$userfile  = addslashes (fread 
 (fopen ($_FILES["userfile"]["tmp_name"], "r"), 
 filesize ($_FILES["userfile"]["tmp_name"]))); 
$file_name = $_FILES["userfile"]["name"]; 
$file_size = $_FILES["userfile"]["size"]; 
$file_type = $_FILES["userfile"]["type"]; 
$date =  date('Y-m-d H:i:s'); //Returns IST 
$user=$session->username;


/* Update hist table */
$sql = "INSERT INTO `hist` (`id`, `link_id` , `table_name`,`field`, `type`, `old_value`, `new_value`, `maint_date`, `user`) VALUES ('', '$id' , 'cfmast','PHOTO', 'UPDATE', 'OLD PHOTO', '$file_name', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());

//update cfmast		   '$userfile', '$file_type', '$file_size', '$date', '$user' )";  
$sqls = "UPDATE cfmast set image= '$userfile', image_type = '$file_type', image_size ='$file_size' where id = ".$id;
$ok1=mysql_query($sqls) or die("ERROR GETTING DATA. REASON: " . mysql_error());
		   if ($ok1)
		   {
		   echo "Record Saved";
		   }
		   else 
		   {
		   echo "<font color = 'red' size = '3'>WARNING:</font> Unable to update. Please contact system administrator";
		   break;
		   }

unset($_POST);
header("Location:customer.php");
    break;
}//switch
}//close else if
}//close isset

?>

										
<div class="controls">
</div>	<!-- /controls -->			
</div> <!-- /control-group -->

                                        
										
											
										 <br />
										
											
										<div class="form-actions">
  
  <a href="property.php"><input name="" type="" class="btn btn-small" value="Back" ></a>

										</div> <!-- /form-actions -->
									</fieldset>
								</form>
                                                          </div>
                                                        </div>	
      		

								</div>
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>