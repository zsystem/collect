<?php
// checking for minimum PHP version
include("include/classes/session.php");
include("include/connection.php");
if (($session->logged_in) == true) {

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WorkCard - Collect+</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/pages/dashboard.css" rel="stylesheet">
<link href="css/datepicker.css" rel="stylesheet">
<link rel="stylesheet" href="public/css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="public/css/style.css" type="text/css">

<link type="text/css" rel="stylesheet" href="libraries/syntaxhighlighter/public/css/shCoreDefault.css">

<script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/XRegExp.js"></script>
<script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/shCore.js"></script>
<script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/shLegacy.js"></script>
<script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/shBrushJScript.js"></script>
<script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/shBrushXML.js"></script>

<script type="text/javascript">
    SyntaxHighlighter.defaults['toolbar'] = false;
    SyntaxHighlighter.all();
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<style type="text/css">
<!--
.a {	color: #FFF;
} 
 body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
}
-->
</style>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.php">Collect+ </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
     <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
        <li class="active"><a href="workcard.php"><i class="icon-edit"></i><span>Work Card</span> </a></li>
           <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
          	            <li><a href="zone.php">Zone</a></li>
            <li><a href="cycle.php">Cycle & Period Code</a></li>
            <li><a href="limit.php">Customer Limit</a></li>
            <li><a href="status.php">Status</a></li>
            <li><a href="severity.php">Feedback Severity</a></li>
			<li><a href="feedback.php">Feedback Status</a></li>

           </ul>
        <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
 <?php if ( ($session->isMaster()) || ($session->isAgent())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
		<li class="active"><a href="workcard.php"><i class=" icon-edit"></i><span>Work Card</span></a></li>
        <li><a href="contract.php"><i class="icon-th-large"></i><span>Contract</span> </a></li>
        <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
     <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<span class="icon-pushpin"></span>
<h3>WorkCard</h3>
          </div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
                        <li><a href="workcard.php">Overview</a></li>
						  <li class="active">
						    <a href="#jscontrols" data-toggle="tab">Active<?php require_once "indicator_a.php";?></a>
						  </li>
                            <li>
						    <a href="w_passive.php">Passive</a>
						  </li>

						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane" id="formcontrols">
 
	              </div>
							 	
								<div class="tab-pane active" id="jscontrols">
 <?php include "active_action.php"; ?>
<div class="accordion-inner">
         
 </div>        
        <div class="span11">

          <div class="widget">
   
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
        </div></div></div>
        <!-- /span6 --> 														  

<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                                                          </div>
                                                        </div>	
      		

								</div>
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<!-- /extra -->
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
          </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->
<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div>
        <!-- /span12 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<script type="text/javascript" src="public/javascript/jquery-1.11.1.js"></script>
<script type="text/javascript" src="public/javascript/zebra_datepicker.js"></script>
<script type="text/javascript" src="public/javascript/core.js"></script>
</body>
</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("views/not_logged_in.php");
}
?>
