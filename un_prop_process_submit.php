<?php
// checking for minimum PHP version
include("include/classes/session.php");
include("include/connection.php");
if (($session->logged_in) && ($session->isAdmin())) {

?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Property - Collect+</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="css/googleapis.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="views/reset.css" type="text/css">
    <link rel="stylesheet" href="views/style.css" type="text/css">
    <link rel="stylesheet" href="views/zebra_pagination.css" type="text/css">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 <style type="text/css">
<!--
.a {	color: #FFF;
} 
 body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
}

	
-->
  </style>
  <style type="text/css">
/* These classes are used by the script as rollover effect for table 1 and 2 */
	
	.tableRollOverEffect1{
		background-color:#317082;
		color:#FFF;
	}

	.tableRollOverEffect2{
		background-color:#000;
		color:#FFF;
	}
	
	.tableRowClickEffect1{
		background-color:#F00;
		color:#FFF;
	}
	.tableRowClickEffect2{
		background-color:#00F;
		color:#FFF;
	}
.countries {
	color: #000000;
}
  </style>
<script src="sorttable.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var arrayOfRolloverClasses = new Array();
	var arrayOfClickClasses = new Array();
	var activeRow = false;
	var activeRowClickArray = new Array();
	
	function highlightTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(this!=activeRow){
			this.setAttribute('origCl',this.className);
			this.origCl = this.className;
		}
		this.className = arrayOfRolloverClasses[tableObj.id];
		
		activeRow = this;
		
	}
	
	function clickOnTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;		
		
		if(activeRowClickArray[tableObj.id] && this!=activeRowClickArray[tableObj.id]){
			activeRowClickArray[tableObj.id].className='';
		}
		this.className = arrayOfClickClasses[tableObj.id];
		
		activeRowClickArray[tableObj.id] = this;
				
	}
	
	function resetRowStyle()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(activeRowClickArray[tableObj.id] && this==activeRowClickArray[tableObj.id]){
			this.className = arrayOfClickClasses[tableObj.id];
			return;	
		}
		
		var origCl = this.getAttribute('origCl');
		if(!origCl)origCl = this.origCl;
		this.className=origCl;
		
	}
		
	function addTableRolloverEffect(tableId,whichClass,whichClassOnClick)
	{
		arrayOfRolloverClasses[tableId] = whichClass;
		arrayOfClickClasses[tableId] = whichClassOnClick;
		
		var tableObj = document.getElementById(tableId);
		var tBody = tableObj.getElementsByTagName('TBODY');
		if(tBody){
			var rows = tBody[0].getElementsByTagName('TR');
		}else{
			var rows = tableObj.getElementsByTagName('TR');
		}
		for(var no=0;no<rows.length;no++){
			rows[no].onmouseover = highlightTableRow;
			rows[no].onmouseout = resetRowStyle;
			
			if(whichClassOnClick){
				rows[no].onclick = clickOnTableRow;	
			}
		}
		
	}


<script type="text/javascript" src="x.js"></script> 
<script type="text/javascript"> 
function adjustLayout()
{
  // Get natural heights
  var cHeight = xHeight("contentcontent");
  var lHeight = xHeight("leftcontent");
  var rHeight = xHeight("rightcontent");
 
  // Find the maximum height
  var maxHeight =
    Math.max(cHeight, Math.max(lHeight, rHeight));
 
  // Assign maximum height to all columns
  xHeight("content", maxHeight);
  xHeight("left", maxHeight);
  xHeight("right", maxHeight);
 
  // Show the footer
  xShow("footer");
}
 
window.onload = function()
{
  xAddEventListener(window, "resize",
    adjustLayout, false);
  adjustLayout();
}
</script>
  </head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		
		    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a>     <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> </div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="active"><a href="zone.php">Zone</a></li>
          </ul>
          <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pushpin"></i>
	      				<h3>Property</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
                        <li ><a href="property.php">Inquiry</a></li>
						  <li class="active">
						    <a href="#formcontrols" data-toggle="tab">Maintenance</a>
						  </li>

						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane" id="formcontrols">

								</div>
								
								<div class="tab-pane active" id="jscontrols">

										

	      			
 
	          <?php
/**
 * Process.php
 * 
 * The Process class is meant to simplify the task of processing
 * user submitted forms, redirecting the user to the correct
 * pages if errors are found, or if form is successful, either
 * way. Also handles the logout procedure.
 */

$mode = "";
$data = "";

$mode = $_REQUEST['button'];
$zone_id = $_REQUEST['zone'];
$lot = $_REQUEST['lot'];
$address1 = $_REQUEST['address1'];
$address2 = $_REQUEST['address2'];
$address3 = $_REQUEST['address3'];
$data = $_REQUEST['sub_menu_id'];
$postcode = $_REQUEST['postcode'];

if(isset($_POST['button'])) {
if (empty($mode)) {
echo 'invalid';
}//close if empty mode
else if ($mode = $_REQUEST['button']) { 
switch ($mode) {
    case "Create   ": 
?>
<h5>Maintenance: Add</h5>
 <div class="accordion-inner">
<?php
 
        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');


$q = "SELECT * FROM `postcode` where postcode = ".$postcode;
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($row=mysql_fetch_array($results)) 
{
date_default_timezone_set("Asia/Kuala_Lumpur");
$date =  date('Y-m-d H:i:s'); //Returns IST 	
$state =  $row['state_code'];	

}
/*$q = "SELECT * FROM `propmast` where address_1 = ".$address1;
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
$rows=mysql_num_rows($results);
if (empty($rows)) {
//$r=$rows["address_1"];*/	
$sql = "INSERT INTO propmast (`id`, `zone_id` ,`lot`, `address_1`, `address_2`, `address_3`, `postcode`, `area`, `state_code`, `date`) VALUES ('', '$zone_id' ,'$lot', '$address1', '$address2', '$address3', '$postcode', '$data', '$state', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());

		   if ($ok)
		   {
		   echo "Record Saved";
		   }
		   else 
		   {
		   echo "<font color = 'red' size = '3'>WARNING:</font> Record Existed";
		   break;
		   }
/*echo $zone_id."<br>";
echo $lot."<br>";
echo $address1."<br>";
echo $address2."<br>";
echo $address3."<br>";
echo $data."<br>";
echo $postcode."<br>";
echo $state."<br>";
echo $date."<br>";*/




?>
 </div>
<?php

	 break;
     case "Save   ":
?>
<h5>Maintenance: Edit</h5>
   <div class="accordion-inner">
<?php
require_once("include/connection.php");
 
        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

$id = $_REQUEST['id'];
$zone_id = $_REQUEST['zone'];
$lot = $_REQUEST['lot'];
$address1 = $_REQUEST['address1'];
$address2 = $_REQUEST['address2'];
$address3 = $_REQUEST['address3'];
$data = $_REQUEST['sub_menu_id'];
$postcode = $_REQUEST['postcode'];

/*$sql = "UPDATE zmast set zone ='$zone', status = '$stat' where id = ".$id;
$ok1=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());
		   if ($ok1)
		   {
		   echo "Record Saved";
		   }
		   else 
		   {
		   echo "<font color = 'red' size = '3'>WARNING:</font> Unable to update. Please contact system administrator";
		   break;
		   }*/
$q = "SELECT * FROM `propmast` where id = ".$id;//check changes
$results = mysql_query ($q) or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($r = mysql_fetch_array($results)) {
//fetching record
$link = $r['id'];
$zone_id = $r['zone_id'];
$lot = $r['lot'];
$address1 = $r['address_1'];
$address2 = $r['address_2'];
$address3 = $r['address_3'];
$data = $r['area'];
$state = $r['state_code'];
$postcode = $r['postcode'];
$data_date = $r['date'];
$date =  date('Y-m-d H:i:s');
//dumping history	
$sql = "INSERT INTO `propmasthist` (`id`, `link`, `zone_id` ,`lot`, `address_1`, `address_2`, `address_3`, `postcode`, `area`, `state_code`, `data_date`, `mntdate`) VALUES ('', '$link', '$zone_id' ,'$lot', '$address1', '$address2', '$address3', '$postcode', '$data', '$state', '$data_date', '$date', '$user')";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());

		   if ($ok)
		   {
			echo 'maint log updated';   
		   }
	
/*$date =  date('Y-m-d H:i:s'); //trial 			   
echo $id."<br>";
echo $zone_id."<br>";
echo ."<br>";
echo $address1."<br>";
echo $address2."<br>";
echo $address3."<br>";
echo $data."<br>";
echo $postcode."<br>";
//echo $state."<br>";
echo $date."<br>";*/
}
?>
 </div>
<?php
	 break;
    case "Delete  ":
?>
 
  </h5> Maintenance: Delete</h5>
 <div class="accordion-inner">
 </div>
<?php
$id = "";
$id = $_POST['id'];
    break;
}//switch
}//close else if
}//close isset

?>

										
<div class="controls">
</div>	<!-- /controls -->			
</div> <!-- /control-group -->

                                        
										
											
										 <br />
										
											
										<div class="form-actions">
  
  <a href="property.php"><input name="" type="" class="btn btn-small" value="Back" ></a>

										</div> <!-- /form-actions -->
									</fieldset>
								</form>
                                                          </div>
                                                        </div>	
      		

								</div>
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>