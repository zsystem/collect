CREATE DATABASE  IF NOT EXISTS `collect` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `collect`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: collect
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `propmast`
--

DROP TABLE IF EXISTS `propmast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `propmast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(3) NOT NULL,
  `lot` varchar(64) NOT NULL,
  `address_1` varchar(64) NOT NULL,
  `address_2` varchar(64) NOT NULL,
  `address_3` varchar(64) DEFAULT NULL,
  `postcode` char(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `area` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `state_code` char(3) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` datetime DEFAULT NULL,
  `last_maint` datetime DEFAULT NULL,
  `contract_id` int(11) DEFAULT NULL,
  `rental` decimal(12,2) unsigned zerofill DEFAULT NULL,
  `last_evaluation` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `propmast`
--

LOCK TABLES `propmast` WRITE;
/*!40000 ALTER TABLE `propmast` DISABLE KEYS */;
INSERT INTO `propmast` VALUES (1,1,'UG-02','Ground Floor, Paradigm Mall','1, Jalan SS7/26A','','47301','Kelana Jaya','SGR','2014-09-16 00:00:00','2014-09-16 23:54:28',1,0000000000.00,'0000-00-00 00:00:00'),(2,1,'CF11','Level C, Paradigm Mall','1, Jalan SS7/26A','','47301','Kelana Jaya','SGR','2014-09-16 00:00:00','2014-09-16 23:03:31',2,0000000000.00,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `propmast` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-12 18:37:14
