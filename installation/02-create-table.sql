SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `cfmast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(120) NOT NULL,
  `zone_id` int(3) NOT NULL,
  `salutation` varchar (12),
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(64) COLLATE utf8_unicode_ci COMMENT 'user''s email, unique',
  `post` varchar(25) NOT NULL,
  `office_no` varchar(12),
  `mobile_no` varchar(12),
  `id_type` varchar(20),
  `id_no` varchar(64),
  `lot` varchar(64) NOT NULL,
  `address_1` varchar(64) NOT NULL,
  `address_2` varchar(64) NOT NULL,
  `address_3` varchar(64),
  `postcode` CHAR(5) NOT NULL COLLATE 'utf8_bin',
  `area` VARCHAR(70) NOT NULL COLLATE 'utf8_bin',
  `state_code` CHAR(3) NOT NULL COLLATE 'utf8_bin',
  `status_id` int(2) NOT NULL,  
  `limit_id` int(2) NOT NULL,
  `image` longblob NOT NULL,
  `image_type` VARCHAR(30) NOT NULL,
  `image_size` bigint(20) NOT NULL default '0',
  `date_added` datetime,
  `user` varchar(64),
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=MyISAM;

CREATE TABLE `hist` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(64) NOT NULL,
  `link_id` int(11) NOT NULL,
  `field` varchar(64) NOT NULL,
  `type` varchar (12) NOT NULL,
  `old_value` varchar (64) NOT NULL,
  `new_value` varchar (64) NOT NULL,
  `maint_date` datetime,
  `user` varchar(64) NOT NULL, 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


CREATE TABLE `postcode` (
    `postcode` CHAR(5) NOT NULL COLLATE 'utf8_bin',
    `area` VARCHAR(70) NOT NULL COLLATE 'utf8_bin',
    `post_office` VARCHAR(30) NOT NULL COLLATE 'utf8_bin',
    `state_code` CHAR(3) NOT NULL COLLATE 'utf8_bin',
    INDEX `idx_postcode` (`postcode`),
    INDEX `idx_state_code` (`state_code`)
)
COLLATE='utf8_bin' ENGINE=MyISAM;
 
CREATE TABLE `state` (
    `state_code` CHAR(3) NOT NULL COLLATE 'utf8_bin',
    `state_name` VARCHAR(35) NOT NULL COLLATE 'utf8_bin',
    INDEX `idx_state_code` (`state_code`)
)
COLLATE='utf8_bin' ENGINE=MyISAM;

CREATE TABLE `statmast` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `status_desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `zmast` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `zone` varchar(64) NOT NULL,
  `zone_desc` varchar(64) NOT NULL,
  `status` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `cycle` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `cycle` varchar(64) NOT NULL,
  `cycle_desc` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `limits` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `floor` decimal(12,2) NOT NULL DEFAULT '0.00',
  `ceiling` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_rate` decimal(25,6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) NOT NULL,
  `period_month` int(2) NOT NULL,
  `period_year` year,
  `contract_id` int(11) NOT NULL,
  `unit_price` decimal(12,2) DEFAULT '0.00',
  `tax_amount` decimal(12,2) DEFAULT '0.00',
  `gross_total` decimal(12,2) DEFAULT '0.00',
  `status` varchar(64) NOT NULL,
  `date_generated` datetime,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `payment` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `invoice_num` varchar(64) NOT NULL,
  `payment_type` varchar(64) NOT NULL,
  `severity_id` int(2) NOT NULL,
  `debit` decimal(12,2) NOT NULL,
  `credit` decimal(12,2) NOT NULL,
  `net` decimal(12,2) NOT NULL,
  `period_month` int(2) NOT NULL,
  `period_year` year,
  `date_received` datetime,
  `collector` varchar(64),
  `remarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `payment_hist` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `invoice_num` varchar(64) NOT NULL,
  `payment_type` varchar(64) NOT NULL,
  `severity_id` int(2) NOT NULL,
  `debit` decimal(12,2) NOT NULL,
  `credit` decimal(12,2) NOT NULL,
  `net` decimal(12,2) NOT NULL,
  `period_month` int(2) NOT NULL,
  `period_year` year,
  `date_received` datetime,
  `collector` varchar(64),
  `remarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `propmast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(3) NOT NULL,
  `lot` varchar(64) NOT NULL,
  `address_1` varchar(64) NOT NULL,
  `address_2` varchar(64) NOT NULL,
  `address_3` varchar(64),
  `postcode` CHAR(5) NOT NULL COLLATE 'utf8_bin',
  `area` VARCHAR(70) NOT NULL COLLATE 'utf8_bin',
  `state_code` CHAR(3) NOT NULL COLLATE 'utf8_bin',
  `date` datetime,
  `last_maint` datetime,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `workmast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collector_id` varchar(30) NOT NULL,
  `zone_id` int(3) NOT NULL,
  `date_assigned` datetime,
  `assigned_by` varchar(30) NOT NULL,
  `status` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `workmast_hist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workmast_id` int(11) NOT NULL,
  `collector_id` varchar(30) NOT NULL,
  `maint_date` datetime,
  `updated_by` varchar(30) NOT NULL,
  `remarks` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `workcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workmast_id` int(11) NOT NULL,
  `prop_id` int(11) NOT NULL,
  `feedback_id` int(3) NOT NULL,
  `remarks` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `workcard_hist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workcard_id` int(11) NOT NULL,
  `maint_date` datetime,
  `updated_by` varchar(30) NOT NULL,
  `remarks` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `timer` (
  `task_id` int(10) unsigned NOT NULL,
  `start` datetime NOT NULL,
  `stop` datetime NOT NULL,
  `spent` int(10) unsigned NOT NULL,
  `manual` tinyint(3) unsigned NOT NULL,
  KEY `task_id` (`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `contract` (
  `contract_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `rental` decimal(12,2) NOT NULL,
  `separate_bil` varchar(1) NOT NULL,
  `tax_id` int(3) NOT NULL,
  `status` int(3) NOT NULL,
  `cycle_id` int(3) NOT NULL,
  `start_date` datetime,
  `end_date` datetime,
  `updated_by` varchar(30) NOT NULL,
  `sales` varchar(30) NOT NULL,
  `remarks` text,
  PRIMARY KEY (`contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='contract';

CREATE TABLE `feedback` (
	`id` int(3) NOT NULL AUTO_INCREMENT,
	`status` varchar(64) NOT NULL,
	`severity_id` int(2) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ----------------------------
-- Future Plan --
-- CREATE TABLE `feed_map` (
-- 	`id` int(3) NOT NULL AUTO_INCREMENT,
-- 	`action` varchar(64) NOT NULL,
-- 	`feed_id` int(3) NOT NULL,
-- 	PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB;
-- ----------------------------


CREATE TABLE `severity` (
	`id` int(3) NOT NULL AUTO_INCREMENT,
	`severity` varchar(64) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;


-- ----------------------------
-- Table structure for active_guests
-- ----------------------------
CREATE TABLE `active_guests` (
  `ip` varchar(15) NOT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for active_users
-- ----------------------------
CREATE TABLE `active_users` (
  `username` varchar(30) NOT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for banned_users
-- ----------------------------
CREATE TABLE `banned_users` (
  `username` varchar(30) NOT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for users
-- ----------------------------
CREATE TABLE `users` (
  `username` varchar(30) NOT NULL,
  `realname` varchar(120) NOT NULL,
  `password` varchar(32) default NULL,
  `userid` varchar(32) default NULL,
  `userlevel` tinyint(1) unsigned NOT NULL,
  `email` varchar(50) default NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `parent_directory` varchar(30) NOT NULL,
  PRIMARY KEY  (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


