<?php
// checking for minimum PHP version
include("include/classes/session.php");
if (($session->logged_in) && ($session->isAdmin())) {
 // database connection details
        $MySQL_host     = 'localhost';
        $MySQL_username = 'root';
        $MySQL_password = '';
        $MySQL_database = 'collect';

        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Zone Maintenance - Collect+</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="css/googleapis.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="views/reset.css" type="text/css">
    <link rel="stylesheet" href="views/style.css" type="text/css">
    <link rel="stylesheet" href="views/zebra_pagination.css" type="text/css">
    
    
    <link href="js/guidely/guidely.css" rel="stylesheet"> 

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<style type="text/css">
<!--
.a {	color: #FFF;
} 
 body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
}

	
-->
  </style>
  <style type="text/css">
/* These classes are used by the script as rollover effect for table 1 and 2 */
	
	.tableRollOverEffect1{
		background-color:#317082;
		color:#FFF;
	}

	.tableRollOverEffect2{
		background-color:#000;
		color:#FFF;
	}
	
	.tableRowClickEffect1{
		background-color:#F00;
		color:#FFF;
	}
	.tableRowClickEffect2{
		background-color:#00F;
		color:#FFF;
	}
.main .main-inner .container .row .span12 #target-1 .widget-content .countries {
	color: #000000;
}
  </style>
<script src="sorttable.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var arrayOfRolloverClasses = new Array();
	var arrayOfClickClasses = new Array();
	var activeRow = false;
	var activeRowClickArray = new Array();
	
	function highlightTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(this!=activeRow){
			this.setAttribute('origCl',this.className);
			this.origCl = this.className;
		}
		this.className = arrayOfRolloverClasses[tableObj.id];
		
		activeRow = this;
		
	}
	
	function clickOnTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;		
		
		if(activeRowClickArray[tableObj.id] && this!=activeRowClickArray[tableObj.id]){
			activeRowClickArray[tableObj.id].className='';
		}
		this.className = arrayOfClickClasses[tableObj.id];
		
		activeRowClickArray[tableObj.id] = this;
				
	}
	
	function resetRowStyle()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(activeRowClickArray[tableObj.id] && this==activeRowClickArray[tableObj.id]){
			this.className = arrayOfClickClasses[tableObj.id];
			return;	
		}
		
		var origCl = this.getAttribute('origCl');
		if(!origCl)origCl = this.origCl;
		this.className=origCl;
		
	}
		
	function addTableRolloverEffect(tableId,whichClass,whichClassOnClick)
	{
		arrayOfRolloverClasses[tableId] = whichClass;
		arrayOfClickClasses[tableId] = whichClassOnClick;
		
		var tableObj = document.getElementById(tableId);
		var tBody = tableObj.getElementsByTagName('TBODY');
		if(tBody){
			var rows = tBody[0].getElementsByTagName('TR');
		}else{
			var rows = tableObj.getElementsByTagName('TR');
		}
		for(var no=0;no<rows.length;no++){
			rows[no].onmouseover = highlightTableRow;
			rows[no].onmouseout = resetRowStyle;
			
			if(whichClassOnClick){
				rows[no].onclick = clickOnTableRow;	
			}
		}
		
	}


<script type="text/javascript" src="x.js"></script> 
<script type="text/javascript"> 
function adjustLayout()
{
  // Get natural heights
  var cHeight = xHeight("contentcontent");
  var lHeight = xHeight("leftcontent");
  var rHeight = xHeight("rightcontent");
 
  // Find the maximum height
  var maxHeight =
    Math.max(cHeight, Math.max(lHeight, rHeight));
 
  // Assign maximum height to all columns
  xHeight("content", maxHeight);
  xHeight("left", maxHeight);
  xHeight("right", maxHeight);
 
  // Show the footer
  xShow("footer");
}
 
window.onload = function()
{
  xAddEventListener(window, "resize",
    adjustLayout, false);
  adjustLayout();
}
</script>
</head>

<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a>     <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> </div>
    <!-- /container -->
  </div>
  <!-- /navbar-inner -->
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li class="active"><a href="zone.php">Zone</a></li>
          </ul>
          <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div>
    <!-- /container -->
  </div>
  <!-- /subnavbar-inner -->
</div>
<!-- /subnavbar -->
    
    
<div class="main">
	
	<div class="main-inner">

    <div class="container">
	
	      <div class="row">	      	
	      	
	      	<div class="span12">
	      		
	      		<div id="target-1" class="widget">
	      			
      			  <div class="widget-content">
	      				
      		<div class="widget-header">
						<i class=" icon-file-text-alt">
						<h3>ZONE</h3></i>
						
					</div> <!-- /widget-header -->
                    	<div class="widget-header">
						&nbsp;<a href="zone.php"><strong>INQUIRY</strong></a> | <a href="zone_edit.php"><strong>MAINTENANCE</strong></a>
						
				</div> <!-- /widget-header -->
   		  </div> <!-- /span6 -->
      		 	
	      	
<div class="widget-content">
	      				
      		<div class="widget-header">
<?php
/**
 * Process.php
 * 
 * The Process class is meant to simplify the task of processing
 * user submitted forms, redirecting the user to the correct
 * pages if errors are found, or if form is successful, either
 * way. Also handles the logout procedure.
 */

$mode = "";

$mode = $_REQUEST['button'];

if(isset($_POST['button'])) {
if (empty($mode)) {
echo 'invalid';
}//close if empty mode
else if ($mode = $_REQUEST['button']) { 
switch ($mode) {
    case "Add   ": 
?>
<i class="icon-file"></i>
<h3>Maintenance: Add</h3>
<?php
	 break;
    case "Edit  ":
?>
<i class="icon-file"></i>
<?php

?>
<?php
	 break;
    case "Delete  ":
?>
  Maintenance: Delete</h3>
<?php
$id = "";
$id = $_POST['id'];
    break;
}//switch
}//close else if
}//close isset

?>
						
		  <h3>Maintenance: Edit</h3>
   		  </div> <!-- /widget-header -->
                    	<div class="widget-header">
						   
	<p class="widget-header">
	  <?php
/**
 * Process.php
 * 
 * The Process class is meant to simplify the task of processing
 * user submitted forms, redirecting the user to the correct
 * pages if errors are found, or if form is successful, either
 * way. Also handles the logout procedure.
 */

$mode = "";

$mode = $_REQUEST['button'];

if(isset($_POST['button'])) {
if (empty($mode)) {
echo 'invalid';
}//close if empty mode
else if ($mode = $_REQUEST['button']) { 
switch ($mode) {
###ADD###
    case "Add   ": 
	break;
    
###EDIT###	
	
	case "Edit  ":
$id = "";
$id = $_REQUEST['id'];
//echo $id;

$query = "SELECT a.id,a.zone,b.status_desc FROM zmast a join statmast b on b.id = a.status where a.id = ".$id."";
$result = mysql_query($query);//now it makes the query and names it as result
while ($r = mysql_fetch_array($result)){
echo '<form action="par_process.php?log='.$id.'" method="post" target="_self" name="button">';
?>
  Zone:<input name="zone" type="text" value"<?php echo $r['zone'];?>"  placeholder="<?php echo $r['zone'];?>" class="login username-field">
 
  </form>  

<?php
}


	 break;
    case "Delete  ":

$id = "";
$id = $_POST['id'];
    break;
}//switch
}//close else if
}//close isset

?>
<br>
<br>
</div> <!-- /widget-header -->
   		  </div>
      		 	
	      	
	      	<div class="widget">
					
					<div class="widget-content">
						<canvas id="pie-chart" class="chart-holder" height="100%" width="100%"></canvas>
					</div> <!-- /widget-content -->
						
				</div> 
      	  <!-- /span3 -->
      	  <!-- /span3 --><!-- /span3 -->
	      	
	      	
      </div> 
	    <!-- /row -->
	
	    </div> <!-- /container -->
    
	</div> <!-- /main-inner -->
	    
</div> <!-- /main --><!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script src="js/guidely/guidely.min.js"></script>


</body>

</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>
