CREATE DATABASE  IF NOT EXISTS `collect` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `collect`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: collect
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(30) NOT NULL,
  `realname` varchar(120) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `userlevel` tinyint(1) unsigned NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `parent_directory` varchar(30) NOT NULL,
  `zone_id` int(3) unsigned NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('Officer1','Nelson Ng','21232f297a57a5a743894a0e4a801fc3','08306876863e906aeea6447f4b8b9460',1,'master1agent1@3g.com',1413110122,'master1',1),('Officer2','Azmir Zakaria','21232f297a57a5a743894a0e4a801fc3','4225cc4e55da0196a7c84a1810b45748',2,'master1agent1member1@3g.com',1223395319,'master1agent1',1),('Officer3','Ariff Saiful','21232f297a57a5a743894a0e4a801fc3','c73446d1f91c4dc8d0e527202a1f3e7e',1,'master1agent2@3g.com',1223395479,'master1',1),('Officer4','Haji Hazizan Daud','21232f297a57a5a743894a0e4a801fc3','9c9f6b9c5c6d8bcdee09b97354cd544e',2,'master1agent2member1@3g.com',1223395477,'master1agent2',1),('Officer5','Azli Effendi Said','21232f297a57a5a743894a0e4a801fc3','695489db84c39d7eb71ab7fcdf889490',1,'master2agent1@3g.com',1223394946,'master2',1),('Officer6','Chen Lee Mee','21232f297a57a5a743894a0e4a801fc3','c40ac57540370897eab305fca804fc2c',2,'master2agent1member1@3g.com',1223395328,'master2agent1',1),('Officer7','Janice Sin Pay Yen','21232f297a57a5a743894a0e4a801fc3','4632fb111729f5e1a363b715702030a6',1,'master2agent2@3g.com',1223395017,'master2',1),('SP1','Mohammad Zaki Mazlan','21232f297a57a5a743894a0e4a801fc3','8b965c7369edb2fce82b0ecb22f4c888',8,'master1@3g.com',1411838918,'master1',1),('SP2','Eddie Chong','21232f297a57a5a743894a0e4a801fc3','d33d6eab248fb2d160f1cecd905a1809',8,'master2@3g.com',1223395358,'master2',1),('SYST','Rozi Bin Othman','21232f297a57a5a743894a0e4a801fc3','24eb1b15240188a500122cf13ada911a',9,'arman@3g.com',1413005694,'admin',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-12 18:37:11
