<!DOCTYPE html>
<html lang="en">
  
 <head>
    <meta charset="utf-8">
    <title>Signup - Collect+</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/googleapis.css" rel="stylesheet">
    
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.php">
				Collect+				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					<li class="">						
						<a href="login.php" class="">
							Already have an account? Login now
						</a>
						
					</li>
					<li class="">						
						<a href="index.php" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container register">
	
	<div class="content clearfix">
		
		<form method="post" action="register.php" name="registerform">
		
			<h1>Employee SignUp</h1>			
			
			<div class="login-fields">
			
				
				<div class="field">
					<label for="username">Username:</label>
					<input type="text" id="login_input_username" required value="" placeholder="User Name" class="login" pattern="[a-zA-Z0-9]{2,64}" name="user_name" />
				</div> <!-- /field -->
			
				<div class="field">
					<label for="login_input_email">Email Address:</label>
					<input type="email" id="login_input_email" value="" placeholder="Email" class="login" name="user_email" required />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="login_input_password_new">Password:</label>
					<input type="password" id="login_input_password_new" name="user_password_new" value="" placeholder="Password" class="login" pattern=".{6,}" required autocomplete="off" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="login_input_password_repeat">Confirm Password:</label>
					<input type="password" id="login_input_password_repeat" name="user_password_repeat" value="" placeholder="Confirm Password" class="login" pattern=".{6,}" required autocomplete="off" />
				</div> <!-- /field -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<!--<span class="login-checkbox">
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
					<label class="choice" for="Field">Agree with the Terms & Conditions.</label>
				</span>-->
									
				<button class="button btn btn-primary btn-large" name="act" id="Register" input type="submit">Register</button>
				
			</div> <!-- .actions -->
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->


<!-- Text Under Box -->
<div class="login-extra">
	Already have an account? <a href="login.php">Login to your account</a>
</div> <!-- /login-extra -->


<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

 </html>
