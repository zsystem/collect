<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Login - Collect+</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/googleapis.css" rel="stylesheet">
    
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">

</head>
</head>

<h1>&nbsp;</h1>
<div class="account-container">
<div align="center"><h1>Employee Login</h1>


<?php
/**
 * User not logged in, display the login form.
 * If user has already tried to login, but errors were
 * found, display the total number of errors.
 * If errors occurred, they will be displayed.
 */
if($form->num_errors > 0){
   echo "<font size=\"2\" color=\"#ff0000\">".$form->num_errors." error(s) found</font>";
}
?>
<!-- <form action="process.php" method="POST"> -->
</div>
</span>

	
  <div class="content clearfix">
<form action="process.php" method="POST"> 
			<div class="login-fields">
				
				<p>Please provide your details</p>
				
				<div class="field">
					<label for="username">Username</label>
				  <input type="text"  name="user" value="<?php echo $form->value("user"); ?>" placeholder="Username" class="login username-field" required/>
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
				  <input type="password"  name="pass" value="<?php echo $form->value("pass"); ?>" placeholder="Password" class="login password-field"  autocomplete="off" required/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
 <!-- <table align="left" border="0" cellspacing="0" cellpadding="3">
  
  <tr><td class="right"></td>
    <td class="right">

      <input type="text" name="user" placeholder="Username" maxlength="30" value="<?php echo $form->value("user"); ?>" >
   
      </td>
    <td class="right"><?php //echo $form->error("user"); ?></td>
  </tr>
  <tr><td class="right"></td>
    <td class="right">
 
      <input type="password" name="pass" maxlength="30" placeholder="Password" value="<?php //echo $form->value("pass"); ?>"  autocomplete="off" required/>

      </td>-->
    <?php 
	echo $form->error("user");
	echo $form->error("pass");
	echo "</br>";?>
   <!-- <input type="checkbox" name="remember" <?php if($form->value("remember") != ""){ echo "checked"; } ?>>
  <font size="2">Remember me next time  -->   
  <input type="hidden" name="sublogin" value="1">
  <!--<font size="2">[<a href="forgotpass.php">Forgot Password?</a>]</font>-->
  <div class="login-actions">
    
    <button class="button btn btn-success btn-large" input type="submit">Login</button>
  </div>
<br>
</form>
</div></div>

<?php

/**
 * Just a little page footer, tells how many registered members
 * there are, how many users currently logged in and viewing site,
 * and how many guests viewing site. Active users are displayed,
 * with link to their user information.
 */
//echo "</td></tr><tr><td align=\"center\"><br><br>";
//echo "<b>Member Total:</b> ".$database->getNumMembers()."<br>";
//echo "There are $database->num_active_users registered members and ";
//echo "$database->num_active_guests guests viewing the site.<br><br>";
//
//include("include/classes/view_active.php");

//echo $session->userlevel;
?>

<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

</html>