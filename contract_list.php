 <div class="accordion-inner">
                                                          
<input type="text" id="search" placeholder="Type to search"> 
                                                          </div></div>
<?php


        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

        // how many records should be displayed on a page?
        $records_per_page = 20;

        // include the pagination class
        require 'Zebra_Pagination.php';

        // instantiate the pagination object
        $pagination = new Zebra_Pagination();

        // set position of the next/previous page links
        $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

        // the MySQL statement to fetch the rows
        // note how we build the LIMIT
        // also, note the "SQL_CALC_FOUND_ROWS"
        // this is to get the number of rows that would've been returned if there was no LIMIT
        // see http://dev.mysql.com/doc/refman/5.0/en/information-functions.html#function_found-rows
        $MySQL = '
		 SELECT
                SQL_CALC_FOUND_ROWS
                a.contract_id, a.property_id, b.name, b.company_name, c.status_desc, a.start_date, a.end_date, a.accno
            FROM
                contract a JOIN cfmast b ON b.id = a.customer_id
				LEFT JOIN statmast c on c.id = a.status
            ORDER BY
                contract_id
            LIMIT
                ' . (($pagination->get_page() - 1) * $records_per_page) . ', ' . $records_per_page . '
        ';


        // if query could not be executed
        if (!($result = @mysql_query($MySQL)))

            // stop execution and display error message
            die(mysql_error());

        // fetch the total number of records in the table
        $rows = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows'));

        // pass the total number of records to the pagination class
        $pagination->records($rows['rows']);

        // records per page
        $pagination->records_per_page($records_per_page);

        ?>

<form name="myForm">
<table border="1" class="table" name="myselect" id="table">
    <thead>
        	<tr>
            <th width="10%" bgcolor="#333">Company Name</th>
            <th width="10%" bgcolor="#333">Contact Person</th>
            <th width="10%" bgcolor="#333">Account Number</th>
 			<th width="3%" bgcolor="#333">Status</th>
            <th width="10%" bgcolor="#333">Start Date</th>
            <th width="10%" bgcolor="#333">End Date</th>
            </tr>

	</thead>
            <?php $index = 0?>

            <?php while ($row = mysql_fetch_assoc($result)):?>
<tbody>

            <tr<?php echo $index++ % 2 ? ' class="even"' : ''?>>
                 <td><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><?php echo $row['company_name']?></a></td>
                 <td><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><?php echo $row['name']?></a></td>
                 <td><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><?php echo $row['accno']?></a></td>
    <?php if ($row['status_desc'] =='ACTIVE') {?><td bgcolor="#00CC66"><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><font color="#000000"><?php echo $row['status_desc']; ?></font></a></td>
    <?php }else if ($row['status_desc'] =='BLOCKED') {?><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><td bgcolor="#FFCC00"><font color="#000000"><?php echo $row['status_desc'];?></font></a></td>
    <?php }else if ($row['status_desc'] =='EXPIRED') {?><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><td bgcolor="#FF0000"><font color="#000000"><?php echo $row['status_desc'];?></font></a></td>
    <?php }else if ($row['status_desc'] =='RENEWED') {?><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><td bgcolor="#339900"><font color="#000000"><?php echo $row['status_desc'];}?></font></a></td>
                  <td><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><?php echo $row['start_date']?></a></td>
                   <td><a href="con_view.php?id=<?php echo $row['contract_id']; ?>"><?php echo $row['end_date']?></a></td>
            </tr>
</tbody>

            <?php endwhile?>
        </table>

        <script type="text/javascript" src="jquery-1.7.2.js"></script>
          <script type="text/javascript" src="views/javascript/zebra_pagination.js"></script>
	      			
      		
      	                                        <div id="collapseOne" class="accordion-body collapse in">
                                                          <div class="accordion-inner">
	          <?php

        // render the pagination links
        $pagination->render();

        ?>