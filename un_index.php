<?php
/**
 * Main.php
 *
 * This is an example of the main page of a website. Here
 * users will be able to login. However, like on most sites
 * the login form doesn't just have to be on the main page,
 * but re-appear on subsequent pages, depending on whether
 * the user has logged in or not.
 *
 * Written by: Jpmaster77 a.k.a. The Grandmaster of C++ (GMC)
 * Last Updated: August 26, 2004
 * Modified by: Arman G. de Castro, October 3, 2008
 * email: armandecastro@gmail.com
 */
include("include/classes/session.php");
echo'haha';
// ... ask if we are logged in here:
if (($session->logged_in) == true) {
    // the user is logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are logged in" view.
   // include("main.php");
if (($session->logged_in) && ($session->isMember())){
      	echo "<h1>Logged In</h1>";
   		echo "Welcome <b>$session->username</b>, you are logged in. <br><br>"
       		."[<a href=\"userinfo.php?user=$session->username\">My Account</a>]   "
       		."[<a href=\"useredit.php\">Edit Account</a>]   ";
	   	echo "[<a href=\"process.php\">Logout</a>]";

} elseif (($session->logged_in) && ($session->isAgent())) {
  		echo "<h1>Logged In</h1>";
   		echo "Welcome <b>$session->username</b>, you are logged in. <br><br>"
       		."[<a href=\"userinfo.php?user=$session->username\">My Account</a>]   "
	   		."[<a href=\"member_register.php?user=$session->username\">Add Member</a>]   "
       		."[<a href=\"useredit.php\">Edit Account</a>]   ";
	    echo "[<a href=\"agent/agent.php\">Agent Center</a>] ";
		echo "[<a href=\"process.php\">Logout</a>]";
		
} elseif (($session->logged_in) && ($session->isMaster())) {
  		echo "<h1>Logged In</h1>";
   		echo "Welcome <b>$session->username</b>, you are logged in. <br><br>"
       		."[<a href=\"userinfo.php?user=$session->username\">My Account</a>]   "
	   		."[<a href=\"agent_register.php?user=$session->username\">Add Agent</a>]   "
       		."[<a href=\"useredit.php\">Edit Account</a>]   ";
	    echo "[<a href=\"master/master.php\">Master Center</a>] ";
		echo "[<a href=\"process.php\">Logout</a>]";

} elseif (($session->logged_in) && ($session->isAdmin())) {
  		echo "<h1>Logged In</h1>";
   		echo "Welcome <b>$session->username</b>, you are logged in. <br><br>"
       		."[<a href=\"userinfo.php?user=$session->username\">My Account</a>]   "
	   		."[<a href=\"master_register.php?user=$session->username\">Add Master</a>]   "
       		."[<a href=\"useredit.php\">Edit Account</a>]   ";
	    echo "[<a href=\"admin/admin.php\">Admin Center</a>] ";
	    echo "[<a href=\"process.php\">Logout</a>]";
}
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
   include("views/not_logged_in.php");
}
?>