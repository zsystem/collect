 <div class="accordion-inner">
                                                          
<input type="text" id="search" placeholder="Type to search"> 
                                                          </div></div>
<?php
include('script_gb.php');

//*** testing PDO ***//
include('dbcon.php');


//*** Main Logic ***//

try {
$stmt = $dbh->prepare('
SELECT 
distinct
a.contract_id, d.severity, h.company_name, g.accno, c.unit_price, c.tax_amount, c.gross_total, (sum(c.unit_price+c.tax_amount)) as TOTAL, a.remarks
FROM workcard a
JOIN workmast b on b.id = a.workmast_id
JOIN invoice c on c.contract_id = a.contract_id
LEFT JOIN severity d on d.id = c.status
LEFT JOIN payment e on e.invoice_num = c.invoice_num
LEFT JOIN payment_hist f on f.invoice_num = c.invoice_num
LEFT JOIN contract g on g.contract_id = a.contract_id
LEFT JOIN cfmast h on h.id = g.customer_id
WHERE a.card = 1
AND b.collector_id = "'.$session->username.'"
AND d.id not in  ("20")
group by a.contract_id, d.severity, g.accno, c.unit_price, c.tax_amount, c.gross_total, a.remarks 
ORDER BY h.company_name');
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        ?>

<form name="myForm">
<table border="1" class="table" name="myselect" id="table">
    <thead>
        	<tr>
            <th width="5%" bgcolor="#333">Invoice Status</th>
            <th width="10%" bgcolor="#333">Account Number</th>
            <th width="10%" bgcolor="#333">Tenant</th>
            <th width="5%" bgcolor="#333">Rental Fees</th>
 			<th width="5%" bgcolor="#333">Tax Amount</th>
            <th width="5%" bgcolor="#333">Total Commitment</th>
            <th width="5%" bgcolor="#333">Total O/S</th>
            </tr>

	</thead>
            <?php $index = 0?>

            <?php  foreach ($result as $row){?>
<tbody>

            <tr<?php echo $index++ % 2 ? ' class="even"' : ''?>>
            	<td><a href="w_active.php?id=<?php echo $row['contract_id']; ?>"><?php echo $row['severity']; ?></a></td>
                 <td><a href="w_active.php?id=<?php echo $row['contract_id']; ?>"><?php echo $row['accno']?></a></td>
                 <td><a href="w_active.php?id=<?php echo $row['contract_id']; ?>"><?php echo $row['company_name']?></a></td>
                 <td><a href="w_active.php?id=<?php echo $row['contract_id']; ?>"><?php echo "RM".(numberfix($row['unit_price']))?></a></td>
                 <td><a href="w_active.php?id=<?php echo $row['contract_id']; ?>"><?php echo "RM".(numberfix($row['tax_amount']))?></a></td>
                  <td><a href="w_active.php?id=<?php echo $row['contract_id']; ?>"><?php echo "RM".(numberfix($row['gross_total']))?></a></td>
                   <td><a href="w_active.php?id=<?php echo $row['contract_id']; ?>"><?php echo "RM".(numberfix($row['TOTAL']))?></a></td>
            </tr>
</tbody>

            <?php 	$dbh = null;

}} catch(PDOException $ex) {
 
    echo $ex->getMessage();
}?>
        </table>
<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
var $rows = $('#table tbody tr');
$('#search').keyup(function () {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function () {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});
</script>
	      			
      		