<?php
// checking for minimum PHP version
include("include/classes/session.php");
include("include/connection.php");
if (($session->logged_in) == true) {


?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Contract - Collect+</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="css/googleapis.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="views/reset.css" type="text/css">
    <link rel="stylesheet" href="views/style.css" type="text/css">
    <link rel="stylesheet" href="views/zebra_pagination.css" type="text/css">
    <link href="css/datepicker.css" rel="stylesheet">
        <link rel="stylesheet" href="public/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="public/css/style.css" type="text/css">

        <link type="text/css" rel="stylesheet" href="libraries/syntaxhighlighter/public/css/shCoreDefault.css">

        <script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/XRegExp.js"></script>
        <script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/shCore.js"></script>
        <script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/shLegacy.js"></script>
        <script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/shBrushJScript.js"></script>
        <script type="text/javascript" src="libraries/syntaxhighlighter/public/javascript/shBrushXML.js"></script>

        <script type="text/javascript">
            SyntaxHighlighter.defaults['toolbar'] = false;
            SyntaxHighlighter.all();
        </script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link href="css/prettify.css" rel="stylesheet">
<script type="text/javascript">
    function noBack() { window.history.forward() }
    noBack();
    window.onload = noBack;
    window.onpageshow = function(evt) { if (evt.persisted) noBack() }
    window.onunload = function() { void (0) }
</script>

        <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/autoNumeric.js"></script>
    <script type="text/javascript">
jQuery(function($) {
    $('.auto').autoNumeric('init');
});
</script>
</head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		
		    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a>     <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> </div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
      		<li><a href="zone.php">Zone</a></li>
            <li><a href="cycle.php">Period Code</a></li>
            <li><a href="limit.php">Customer Limit</a></li>
            <li><a href="status.php">Status</a></li>
            <li><a href="severity.php">Feedback Severity</a></li>
			<li><a href="feedback.php">Feedback Status</a></li>


          </ul>
          <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?> <?php if ( ($session->isMaster()) || ($session->isAgent())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>	
		 <li><a href="workcard.php"><i class=" icon-edit"></i><span>Work Card</span></a></li><li class="active"><a href="contract.php"><i class="icon-th-large"></i><span>Contract</span> </a></li>
        <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    

<div class="main">
	
  <div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pushpin"></i>
	      				<h3>Customer Contract</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
                        <li ><a href="contract.php">Inquiry</a></li>
						  <li class="active">
						    <a href="#formcontrols" data-toggle="tab">Maintenance</a>
						  </li>

						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane" id="formcontrols">

								</div>
								
								<div class="tab-pane active" id="jscontrols">

<div class="control-group">											

	      			
 
	          <?php
/**
 * Process.php
 * 
 * The Process class is meant to simplify the task of processing
 * user submitted forms, redirecting the user to the correct
 * pages if errors are found, or if form is successful, either
 * way. Also handles the logout procedure.
 */

$mode = "";

$mode = $_REQUEST['button'];


if(isset($_POST['button'])) {
if (empty($mode)) {
echo 'invalid';
}//close if empty mode
else if ($mode = $_REQUEST['button']) { 
switch ($mode) {
    case "Add   ": 
?>
<h5>Maintenance: Add</h5>
 <div class="accordion-inner">
                                                          
<form action="con_search.php" method="post">
 COMPANY NAME:  <input name="co" type="text" required>
</select>
<input name="button" type="submit" class=" btn-info" value="Search"> 
              </form>
            </div></div>

 <form action="con_process.php" method="post" enctype="multipart/form-data"> 
<div class="control-group">											
<?php

        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

        // how many records should be displayed on a page?
        $records_per_page = 20;

        // include the pagination class
        require 'Zebra_Pagination.php';

        // instantiate the pagination object
        $pagination = new Zebra_Pagination();

        // set position of the next/previous page links
        $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

        // the MySQL statement to fetch the rows
        // note how we build the LIMIT
        // also, note the "SQL_CALC_FOUND_ROWS"
        // this is to get the number of rows that would've been returned if there was no LIMIT
        // see http://dev.mysql.com/doc/refman/5.0/en/information-functions.html#function_found-rows
        $MySQL = '
            SELECT
                SQL_CALC_FOUND_ROWS
                *
            FROM
                cfmast WHERE zone_id ='.$branch_id.' 
            ORDER BY
                id 
            LIMIT
                ' . (($pagination->get_page() - 1) * $records_per_page) . ', ' . $records_per_page . '
        ';


        // if query could not be executed
        if (!($result = @mysql_query($MySQL)))

            // stop execution and display error message
            die(mysql_error());

        // fetch the total number of records in the table
        $rows = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows'));

        // pass the total number of records to the pagination class
        $pagination->records($rows['rows']);

        // records per page
        $pagination->records_per_page($records_per_page);

        ?>            <table border="1" class="table" name="target-1">
<thead>
        	<tr>
            <th width="2%" bgcolor="#333" class="widget-table">Select</th>
             <th width="20%" bgcolor="#000066">Company Name</th>
            <th width="20%" bgcolor="#333">Contact Person</th>
            <th width="20%" bgcolor="#333">E-Mail</th>
 			<th width="20%" bgcolor="#333">Office Number</th>            </tr>
</thead>
            <?php $index = 0?>

            <?php while ($row = mysql_fetch_assoc($result)):?>

            <tr<?php echo $index++ % 2 ? ' class="even"' : ''?>>
                <td><input name="id" type="radio" value="<?php echo $row["id"];?>"/></td>
                 <td><?php echo $row['company_name']?></a></td>
                 <td><?php echo $row['salutation'].' '.$row['name']?></a></td>
                 <td><?php echo $row['user_email']?></a></td>
                 <td><?php echo $row['office_no']?></a></td>
            </tr>

            <?php endwhile?>
        </table>

        <script type="text/javascript" src="jquery-1.7.2.js"></script>
          <script type="text/javascript" src="views/javascript/zebra_pagination.js"></script>
	      			
  <div class="accordion-inner">
	          <?php

        // render the pagination links
        $pagination->render();

        ?>
        <div class="form-actions">
                                        <script type="text/javascript">
    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton1').removeAttr('disabled');
                }
            else {
                $('#submitButton1').attr('disabled', 'disabled');
                }
            });
        });
	    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton2').removeAttr('disabled');
                }
            else {
                $('#submitButton2').attr('disabled', 'disabled');
                }
            });
        });
			    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton0').attr('disabled', 'disabled');
                }
            else {
                $('#submitButton0').removeAttr('disabled');
                }
            });
        });
</script>    
  <input name="button" type="submit" class="btn btn-small" value="Proceed  " disabled id="submitButton1"> 
 <a href="con	_edit.php" role="button" class="btn btn-small">Back</a>
 </div><!-- /form-actions -->
									</fieldset>
					<?php

    break;
    case "Proceed  ":
	?>
<form action="con_process_submit.php" method="post" enctype="multipart/form-data"> 
     <?php
$id = "";
$ids = $_REQUEST['id'];
?>
<h5>Maintenance: Add</h5>
 <div class="accordion-inner" id="products_modal_box">
 <table width="84%" border="0" cellpadding="1">
      <tr>
        <th width="23%" align="left" valign="middle" scope="row">ZONE:</th>
        <td width="29%" align="left"><?php
 $q1 = "SELECT id, zone, zone_desc FROM `zmast` where status = 1 AND id = '".$branch_id."'";
$result_post = mysql_query ($q1) or die("ERROR GETTING DATA. REASON: " . mysql_error());
?>
            <?php
while ($row = mysql_fetch_array($result_post)) {
 ?>
<input name="zo" id="zo" type="text" value="<?php echo $row['zone'];?> - <?php echo $row['zone_desc']; }?>" size="120" maxlength="120" autocomplete="off" readonly/></td>
      
        <th width="14%" align="left" scope="col">&nbsp;</th>
        <th width="34%" align="left" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">PROPERTY ID:</th>
        <td align="left"><input name="prop" id="prop" type="text" value="" size="120" maxlength="120" autocomplete="off" required/></td>
        <td align="left"><a href="#myModal" role="button" class="btn btn-info" data-toggle="modal">Search</a></td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">COMPANY NAME</th>
        <?php
		$q = " SELECT * from cfmast WHERE id = ".$ids;
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($r=mysql_fetch_array($results)) {
$id = $r['id'];
$co = $r['company_name'];
$name = $r['name'];
$mail = $r['user_email'];
$post = $r['post'];
$idtype = $r['id_type'];
$idnum = $r['id_no'];
$office = $r['office_no'];
$mobile = $r['mobile_no'];
$image = $r['image'];
$img_type = $r['image_type'];
$size = $r['image_size'];
$date = $r['date_added'];
$alert = $r['alert'];
?>
        		
        <td align="left"><input name="company" type="text" value="<?php echo( htmlspecialchars( $co ) );}?>" size="120" maxlength="120" autocomplete="off" readonly/><input type="hidden" value="<?php echo( htmlspecialchars( $id ) );?>" name="com"/></td>

        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">MONTHLY RENTAL (RM):</th>
        <td align="left"><input name="rent" id="rent" type="text" class="auto"  data-a-sign="" required/></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">BILLING CYCLE:</th>
        <td align="left"><select name="billing" required>
          <option value="1">Weekly</option>
          <option value="2">Fortnightly</option>
          <option value="3" selected="selected">Monthly</option>
          <option value="4">Quarterly</option>
          <option value="5">Half Yearly</option>
          <option value="6">Yearly</option>
        </select></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">EXCLUDE BILL:</th>
        <td align="left">
         Yes <input name="bill" type="radio" id="radio" value="Y" checked>&nbsp;
         No <input name="bill" type="radio" id="radio" value="N">
        </td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="col">CONTRACT START DATE:</th>
        <th align="left" scope="col"> <input class"span2" value="" id="datepicker-example7-start" type="text" name ="dp2" required></th>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="col">CONTRACT EXPIRY DATE:</th>
        <th align="left" scope="col"> <input class"span2" value="" id="datepicker-example7-end" type="text" name ="dp3" required></th>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">PREFERRED CONTACT:</th>
        <td align="left">
        <select name="alert" id="type" required>
<?php if ($alert=='P'){?><option value="P" selected="selected">Phone</option><?php } else {?><option value="P">Phone</option><?php }?>
<?php if ($alert=='M'){?><option value="M" selected="selected">E-Mail</option><?php } else {?><option value="M">E-Mail</option><?php }?>
<?php if ($alert=='B'){?><option value="B" selected="selected">Both</option><?php } else {?><option value="B">Both</option><?php }?>
<?php if ($alert=='N'){?><option value="N" selected="selected">No Contact</option><?php } else {?><option value="N">No Contact</option><?php }?>
          &nbsp;</select></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
    </table>
 </div>
 <div class="form-actions">

  <input name="button" type="submit" class="btn btn-small" value="Create   " id="submitButton0"> 
   <input name="button" class="btn btn-small" type="reset"   value="Reset    ">   
   <a href="contract.php" role="button" class="btn btn-small">Back</a>
   
			  </div> <!-- /form-actions -->
<?php
break;
}//switch
}//close else if
}//close isset

?>

										
<div class="controls">

                                                    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><font size="2em">Close</font></button>
                                                        <h3 id="myModalLabel">PROPERTY SEARCH</h3>
                                                      </div>

                                                        <div class="modal-body">
                                                        <input type="text" id="search" placeholder="Type to search">
  <?php

        // database connection details

        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

        // how many records should be displayed on a page?
        $records_per_page = 20;

        // include the pagination class
        require 'Zebra_Pagination.php';

        // instantiate the pagination object
        $pagination = new Zebra_Pagination();

        // set position of the next/previous page links
        $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

        // the MySQL statement to fetch the rows
        // note how we build the LIMIT
        // also, note the "SQL_CALC_FOUND_ROWS"
        // this is to get the number of rows that would've been returned if there was no LIMIT
        // see http://dev.mysql.com/doc/refman/5.0/en/information-functions.html#function_found-rows
         $MySQL = '
            SELECT distinct
                SQL_CALC_FOUND_ROWS
a.id, z.zone, a.lot, a.address_1, a.address_2, a.address_3, a.postcode, a.area,  a.date
            FROM
                propmast a 
				left outer join zmast z on z.id = a.zone_id
				WHERE a.zone_id = '.$branch_id.' and contract_id = 0
            ORDER BY
                a.id
        ';


        // if query could not be executed
        if (!($result = @mysql_query($MySQL)))

            // stop execution and display error message
            die(mysql_error());

        // fetch the total number of records in the table
        $rows = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows'));


        ?>                                                      
                                                        

                                                      </div>
<div class="modal-body">
<p>
<form name="myForm">
<table border="1" class="table" name="myselect" id="table">
    <thead>
        	<tr>
            <th width="2%" bgcolor="#333" class="widget-table">Selection</th>
             <th width="8%" bgcolor="#333">Lot</th>
            <th width="30%" bgcolor="#333">Address 1</th>
            <th width="20%" bgcolor="#333">Address 2</th>
            <th width="12%" bgcolor="#333">Area</th>
            </tr>
    </thead>
            <?php $index = 0?>

            <?php while ($row = mysql_fetch_assoc($result)):1; $re = $row["id"];$lo = $row["lot"];			?>
 <tbody>
            <tr<?php echo $index++ % 2 ? ' class="even"' : ''?>>
                <!--<td><input name="id" type="radio" value="<?php echo $row["id"];?>" onChange="selectItem();"/></td>-->
                               <?php print "<td><a href=\"javascript:;\" onclick=\"$('#prop').val('".$re."');$('#products_modal_box').dialog('close');\">Select</a><input type = 'hidden' value= 'prop' id='prop2'></td>";?><!--pass value to parent -->
                 <td><?php echo $row['lot']?></td>
                 <td><?php echo $row['address_1']?></td>
                 <td><?php echo $row['address_2']?></td>
                 <td><?php echo $row['area']?></td>
            </tr>
</tbody>
            <?php endwhile?>
        </table>

        <script type="text/javascript" src="jquery-1.7.2.js"></script>
          <script type="text/javascript" src="views/javascript/zebra_pagination.js"></script>

</p>
                                                      </div>
                                                      <div class="modal-body"><p></p></div>
                                                      <div class="modal-body"><p></p></div>
                                                      <div class="modal-body"><p></p></div>
                                                      <div class="modal-footer">
                                                        
                                                      </div>
                                                    </div>
										</div> <!-- /form-actions -->
</div>	<!-- /controls -->			
</div> <!-- /control-group -->

                                        
										
											
										 <br />
										
											
										
			  </form></form></form>
                                </form></form>
            </div>
          </div>	
      		

</div>
								
				  </div>
						  
						  
			  </div>
						
						
						
						
						
			</div> <!-- /widget-content -->
						
		  </div> <!-- /widget -->
	      		
      </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
  </div> <!-- /row -->
	
</div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
        <script type="text/javascript" src="public/javascript/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="public/javascript/zebra_datepicker.js"></script>
        <script type="text/javascript" src="public/javascript/core.js"></script>  </body>

</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>