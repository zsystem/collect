<?php
// checking for minimum PHP version
include("include/classes/session.php");
include("include/connection.php");
if (($session->logged_in) == true) {

?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Property Details - Collect+</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="css/googleapis.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="views/reset.css" type="text/css">
    <link rel="stylesheet" href="views/style.css" type="text/css">
    <link rel="stylesheet" href="views/zebra_pagination.css" type="text/css">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 <style type="text/css">
<!--
.a {	color: #FFF;
} 
 body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
}

	
-->
  </style>
  <style type="text/css">
/* These classes are used by the script as rollover effect for table 1 and 2 */
	
	.tableRollOverEffect1{
		background-color:#317082;
		color:#FFF;
	}

	.tableRollOverEffect2{
		background-color:#000;
		color:#FFF;
	}
	
	.tableRowClickEffect1{
		background-color:#F00;
		color:#FFF;
	}
	.tableRowClickEffect2{
		background-color:#00F;
		color:#FFF;
	}
.countries {
	color: #000000;
}
  </style>
<script src="sorttable.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var arrayOfRolloverClasses = new Array();
	var arrayOfClickClasses = new Array();
	var activeRow = false;
	var activeRowClickArray = new Array();
	
	function highlightTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(this!=activeRow){
			this.setAttribute('origCl',this.className);
			this.origCl = this.className;
		}
		this.className = arrayOfRolloverClasses[tableObj.id];
		
		activeRow = this;
		
	}
	
	function clickOnTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;		
		
		if(activeRowClickArray[tableObj.id] && this!=activeRowClickArray[tableObj.id]){
			activeRowClickArray[tableObj.id].className='';
		}
		this.className = arrayOfClickClasses[tableObj.id];
		
		activeRowClickArray[tableObj.id] = this;
				
	}
	
	function resetRowStyle()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(activeRowClickArray[tableObj.id] && this==activeRowClickArray[tableObj.id]){
			this.className = arrayOfClickClasses[tableObj.id];
			return;	
		}
		
		var origCl = this.getAttribute('origCl');
		if(!origCl)origCl = this.origCl;
		this.className=origCl;
		
	}
		
	function addTableRolloverEffect(tableId,whichClass,whichClassOnClick)
	{
		arrayOfRolloverClasses[tableId] = whichClass;
		arrayOfClickClasses[tableId] = whichClassOnClick;
		
		var tableObj = document.getElementById(tableId);
		var tBody = tableObj.getElementsByTagName('TBODY');
		if(tBody){
			var rows = tBody[0].getElementsByTagName('TR');
		}else{
			var rows = tableObj.getElementsByTagName('TR');
		}
		for(var no=0;no<rows.length;no++){
			rows[no].onmouseover = highlightTableRow;
			rows[no].onmouseout = resetRowStyle;
			
			if(whichClassOnClick){
				rows[no].onclick = clickOnTableRow;	
			}
		}
		
	}


<script type="text/javascript" src="x.js"></script> 
<script type="text/javascript"> 
function adjustLayout()
{
  // Get natural heights
  var cHeight = xHeight("contentcontent");
  var lHeight = xHeight("leftcontent");
  var rHeight = xHeight("rightcontent");
 
  // Find the maximum height
  var maxHeight =
    Math.max(cHeight, Math.max(lHeight, rHeight));
 
  // Assign maximum height to all columns
  xHeight("content", maxHeight);
  xHeight("left", maxHeight);
  xHeight("right", maxHeight);
 
  // Show the footer
  xShow("footer");
}
 
window.onload = function()
{
  xAddEventListener(window, "resize",
    adjustLayout, false);
  adjustLayout();
}
</script>
  </head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		
		    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a>     <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> </div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
      		            <li><a href="zone.php">Zone</a></li>
            <li><a href="cycle.php">Cycle & Period Code</a></li>
            <li><a href="limit.php">Customer Limit</a></li>
            <li><a href="status.php">Status</a></li>
            <li><a href="severity.php">Feedback Severity</a></li>
			<li><a href="feedback.php">Feedback Status</a></li>
          </ul>
          <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?> <?php if ( ($session->isMaster()) || ($session->isAgent())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>	
		 <li><a href="workcard.php"><i class=" icon-edit"></i><span>Work Card</span></a></li><li><a href="contract.php"><i class="icon-th-large"></i><span>Contract</span> </a></li>
        <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li  class="active"><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<span class="icon-pushpin"></span>
<h3>Property Management</h3>
          </div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
                        <li  class="active"><a href="#jscontrols">Inquiry</a></li>
						  <li>
						    <a href="property_edit.php">Maintenance</a>
						  </li>

						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane" id="formcontrols">
 
	              </div>
								
								<div class="tab-pane active" id="jscontrols">
<label class="control-label"><h3>Record Details</h3></label>
<?php
$id = $_REQUEST['id'];

$q = "SELECT a.id, a.zone_id, a.lot, a.address_1, a.address_2, a.address_3, a.area, a.state_code, a.postcode, a.date, a.contract_id, a.rental, a.last_evaluation, c.company_name, d.status_desc, b.start_date, b.end_date, b.sales
FROM `propmast` a
LEFT JOIN `contract` b ON b.contract_id = a.contract_id
LEFT JOIN `cfmast` c ON c.id = b.customer_id
LEFT JOIN `statmast` d ON d.id = b.status
WHERE a.id = ".$id;//retrieve state mapping
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($r=mysql_fetch_array($results)) {
$link = $r['id'];
$pzone_id = $r['zone_id'];
$lot = $r['lot'];
$address1 = $r['address_1'];
$address2 = $r['address_2'];
$address3 = $r['address_3'];
$data = $r['area'];
$state = $r['state_code'];
$postcode = $r['postcode'];
$date =  $r['date'];
$prop_stat = $r['contract_id'];
$rental = $r['rental'];
$last_evaluation = $r['last_evaluation'];
$company_name = $r['company_name'];
$status_desc = $r['status_desc'];
$start_date = $r['start_date'];
$end_date = $r['end_date'];
$sales = $r['sales'];
?>
<table width="98%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <th width="8%" scope="col" align="left">RECORD NO:</th>
    <td width="29%"><?php echo "".$id."";?></td>
    <td width="15%"><strong>CONTRACT ID:</strong></td>
    <td width="27%"><?php echo $prop_stat; ?></td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr>
    <th scope="row"  align="left">LOT/NO:</th>
    <td><?php echo "".$lot."";?></td>
    <td width="15%"><strong>CURRENT TENANT:</strong></td>
    <td><?php echo $company_name; ?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row"  align="left">ADDRESS 1:</th>
    <td><?php echo "".$address1."";?></td>
    <td><strong>CONTRACT STATUS:</strong></td>
    <?php if ($status_desc =='ACTIVE') {?><td width="27%" bgcolor="#00CC66"><font color="#FFFFFF"><?php echo $status_desc; ?></font></td>
    <?php }else if ($status_desc =='BLOCKED') {?><td width="9%" bgcolor="#FFCC00"><font color="#FFFFFF"><?php echo $status_desc;?></font></td>
    <?php }else if ($status_desc =='EXPIRED') {?><td width="6%" bgcolor="#FF0000"><font color="#FFFFFF"><?php echo $status_desc;?></font></td>
    <?php }else if ($status_desc =='RENEWED') {?><td width="6%" bgcolor="#339900"><font color="#FFFFFF"><?php echo $status_desc;}?></font></td>
  </tr>
  <tr>
    <th scope="row"  align="left">ADDRESS 2:</th>
    <td><?php echo "".$address2."";?></td>
    <td><strong>CONTRACT START DATE:</strong></td>
    <td><?php echo $start_date; ?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row"  align="left">ADDRESS 3:</th>
    <td><?php echo "".$address3."";?></td>
    <td><strong>CONTRACT END DATE:</strong></td>
    <td><?php echo $end_date; ?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row"  align="left">AREA:</th>
    <td><?php echo "".$data."";?></td>
    <td><strong>DEFAULT RENTAL FEES:</strong></td>
    <td>RM <?php echo( htmlspecialchars( numberfix($r['rental'])) );?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row"  align="left">POSTCODE:</th>
    <td><?php echo "".$postcode."";?></td>
    <td><strong>EVALUATION DATE:</strong></td>
    <td><?php echo $last_evaluation; ?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row"  align="left">STATE:</th>
    <td><?php $sql = "SELECT distinct state_name FROM `state` where `state_code` = '$state'";//populate state reference
$test = mysql_query ($sql)or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($re=mysql_fetch_array($test)) {
echo "".$re['state_name']."";
}?></td>
    <td><strong>CONTRACT ADDED BY:</strong></td>
    <td><?php echo $sales; ?></td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <th scope="row"  align="left">DATE ADDED:</th>
    <td><?php echo "".$date.""; }
?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row"  align="left">STATUS:</th>
    <td><?php 
	if ($prop_stat = 0) {echo "VACANT";}
	else {echo "RENTED";}?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>












<div class="form-actions">                                       
 <a href="property.php" role="button" class="btn btn-small">Back</a>
</div> <!-- /form-actions -->
 <div class="control-group">
												<div class="controls">
                                                
													 <div class="accordion" id="accordion2">
                                                      <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                                            Maintenance Log
                                                          </a>
                                                        </div>
                                                        <div id="collapseOne" class="accordion-body collapse in">
                                                          <div class="accordion-inner">
<?php

$id = $_REQUEST['id'];
        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

        // how many records should be displayed on a page?
        $records_per_page = 20;

        // include the pagination class
        require 'Zebra_Pagination.php';

        // instantiate the pagination object
        $pagination = new Zebra_Pagination();

        // set position of the next/previous page links
        $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

        // the MySQL statement to fetch the rows
        // note how we build the LIMIT
        // also, note the "SQL_CALC_FOUND_ROWS"
        // this is to get the number of rows that would've been returned if there was no LIMIT
        // see http://dev.mysql.com/doc/refman/5.0/en/information-functions.html#function_found-rows
        $MySQL = '
            SELECT
                SQL_CALC_FOUND_ROWS
                id, table_name, link_id, field, type, old_value, new_value, maint_date, user
            FROM
                hist 
			WHERE `table_name` = "propmast"  AND link_id = "'.$id.'"
            ORDER BY
                id
            LIMIT
                ' . (($pagination->get_page() - 1) * $records_per_page) . ', ' . $records_per_page . '
        ';


        // if query could not be executed
        if (!($result = @mysql_query($MySQL)))

            // stop execution and display error message
            die(mysql_error());

        // fetch the total number of records in the table
        $rows = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows'));

        // pass the total number of records to the pagination class
        $pagination->records($rows['rows']);

        // records per page
        $pagination->records_per_page($records_per_page);

        ?>       <table border="1" class="table" name="target-1">
       <thead>
        	<tr>
             <th width="10%" bgcolor="#333">Field Name</th>
            <th width="5%" bgcolor="#333">Type</th>
            <th width="20%" bgcolor="#333">Old Record</th>
            <th width="20%" bgcolor="#333">New Record</th>
              <th width="10%" bgcolor="#333">Maintenance Date</th>
            <th width="10%" bgcolor="#333">Maintained By</th>
            </tr>
		</thead>	

            <?php $index = 0?>

            <?php while ($row = mysql_fetch_assoc($result)):1			?>

            <tr<?php echo $index++ % 2 ? ' class="even"' : ''?>>
                 <td><?php echo $row['field']?></td>
                 <td><?php echo $row['type']?></td>
                 <td><?php echo $row['old_value']?></td>
                 <td><?php echo $row['new_value']?></td>
                                  <td><?php echo $row['maint_date']?></td>
                 <td><?php echo $row['user']?></td>
            </tr>

            <?php endwhile?>
        </table>
     

        <script type="text/javascript" src="jquery-1.7.2.js"></script>
          <script type="text/javascript" src="views/javascript/zebra_pagination.js"></script>
	      			
      		
      	                                        <div id="collapseOne" class="accordion-body collapse in">
                                                          <div class="accordion-inner">
	          <?php

        // render the pagination links
        $pagination->render();

        ?>


                                                          </div>
                                                        </div>
                                                      </div>
                                                      
                                                      </div>
                                                    </div>
												</div> <!-- /controls -->	
											</div> <!-- /control-group -->
                                                          </div>
                                                        </div>	
      		

								</div>
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>