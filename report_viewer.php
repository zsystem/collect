<?php 
include('include/connection.php');
include('script_gb.php');

//*** testing PDO ***//
include('dbcon.php');


//*** Main Logic ***//

try {
	
	$stmt = $dbh->prepare("SELECT report_id, report_desc FROM report");
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	print '<table border="1" class="table" name="target-1">';
	print '<thead>';
	print '<tr>';
	print '<th> Report ID </th>';
	print '<th> Report Description </th>';
	print '</tr>';
	print '</thead>';

    /*** loop over the results ***/
    foreach ($result as $row)
        {
        print '<tr><td>' . $row['report_id'] .' </td> <td> '. $row['report_desc'];
        }
		
	print '</table>';
		
	/*** close connection ***/
	$dbh = null;

} catch(PDOException $ex) {
 
    echo $ex->getMessage();
}
 
 ?>