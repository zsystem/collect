<?php
// checking for minimum PHP version
include("include/classes/session.php");
include("include/connection.php");
if (($session->logged_in) == true) {


?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Customer - Collect+</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="views/reset.css" type="text/css">
    <link rel="stylesheet" href="views/style.css" type="text/css">
    <link rel="stylesheet" href="views/zebra_pagination.css" type="text/css">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 <style type="text/css">
<!--
.a {
	color: #FFF;
}

	
-->
  </style>
  <style type="text/css">
/* These classes are used by the script as rollover effect for table 1 and 2 */
	
	.tableRollOverEffect1{
		background-color:#317082;
		color:#FFF;
	}

	.tableRollOverEffect2{
		background-color:#000;
		color:#FFF;
	}
	
	.tableRowClickEffect1{
		background-color:#F00;
		color:#FFF;
	}
	.tableRowClickEffect2{
		background-color:#00F;
		color:#FFF;
	}
.countries {
	color: #000000;
}
  </style>
<script src="sorttable.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var arrayOfRolloverClasses = new Array();
	var arrayOfClickClasses = new Array();
	var activeRow = false;
	var activeRowClickArray = new Array();
	
	function highlightTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(this!=activeRow){
			this.setAttribute('origCl',this.className);
			this.origCl = this.className;
		}
		this.className = arrayOfRolloverClasses[tableObj.id];
		
		activeRow = this;
		
	}
	
	function clickOnTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;		
		
		if(activeRowClickArray[tableObj.id] && this!=activeRowClickArray[tableObj.id]){
			activeRowClickArray[tableObj.id].className='';
		}
		this.className = arrayOfClickClasses[tableObj.id];
		
		activeRowClickArray[tableObj.id] = this;
				
	}
	
	function resetRowStyle()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(activeRowClickArray[tableObj.id] && this==activeRowClickArray[tableObj.id]){
			this.className = arrayOfClickClasses[tableObj.id];
			return;	
		}
		
		var origCl = this.getAttribute('origCl');
		if(!origCl)origCl = this.origCl;
		this.className=origCl;
		
	}
		
	function addTableRolloverEffect(tableId,whichClass,whichClassOnClick)
	{
		arrayOfRolloverClasses[tableId] = whichClass;
		arrayOfClickClasses[tableId] = whichClassOnClick;
		
		var tableObj = document.getElementById(tableId);
		var tBody = tableObj.getElementsByTagName('TBODY');
		if(tBody){
			var rows = tBody[0].getElementsByTagName('TR');
		}else{
			var rows = tableObj.getElementsByTagName('TR');
		}
		for(var no=0;no<rows.length;no++){
			rows[no].onmouseover = highlightTableRow;
			rows[no].onmouseout = resetRowStyle;
			
			if(whichClassOnClick){
				rows[no].onclick = clickOnTableRow;	
			}
		}
		
	}


<script type="text/javascript" src="x.js"></script> 
<script type="text/javascript"> 
function adjustLayout()
{
  // Get natural heights
  var cHeight = xHeight("contentcontent");
  var lHeight = xHeight("leftcontent");
  var rHeight = xHeight("rightcontent");
 
  // Find the maximum height
  var maxHeight =
    Math.max(cHeight, Math.max(lHeight, rHeight));
 
  // Assign maximum height to all columns
  xHeight("content", maxHeight);
  xHeight("left", maxHeight);
  xHeight("right", maxHeight);
 
  // Show the footer
  xShow("footer");
}
 
window.onload = function()
{
  xAddEventListener(window, "resize",
    adjustLayout, false);
  adjustLayout();
}
</script>
  </head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		
		    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a>     <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> </div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
      		            <li><a href="zone.php">Zone</a></li>
            <li><a href="cycle.php">Cycle & Period Code</a></li>
            <li><a href="limit.php">Customer Limit</a></li>
            <li><a href="property.php">Property</a></li>
            <li><a href="status.php">Status</a></li>
            <li><a href="severity.php">Feedback Severity</a></li>
			<li><a href="feedback.php">Feedback Status</a></li>
         </ul>
          <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li class="active"><a href="#"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?> <?php if ( ($session->isMaster()) || ($session->isAgent())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>	
		 <li><a href="workcard.php"><i class=" icon-edit"></i><span>Work Card</span></a></li><li><a href="contract.php"><i class="icon-th-large"></i><span>Contract</span> </a></li>
        <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li class="active"><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pushpin"></i>
	      				<h3>Customer Profile</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
                        <li ><a href="customer.php">Inquiry</a></li>
						  <li class="active">
						    <a href="#formcontrols" data-toggle="tab">Maintenance</a>
						  </li>

						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane" id="formcontrols">

								</div>
								
								<div class="tab-pane active" id="jscontrols">

 <form action="cus_process_submit.php" method="post" enctype="multipart/form-data"> 
<div class="control-group">											

	      			
 
	          <?php
/**
 * Process.php
 * 
 * The Process class is meant to simplify the task of processing
 * user submitted forms, redirecting the user to the correct
 * pages if errors are found, or if form is successful, either
 * way. Also handles the logout procedure.
 */

$mode = "";

$mode = $_REQUEST['button'];


if(isset($_POST['button'])) {
if (empty($mode)) {
echo 'invalid';
}//close if empty mode
else if ($mode = $_REQUEST['button']) { 
switch ($mode) {
    case "Add   ": 
?>
<h5>Maintenance: Add</h5>
 <div class="accordion-inner">
 <table width="91%" border="0" cellpadding="0" cellspacing="2">
      <tr>
        <th colspan="4" align="left" valign="middle" scope="row"> <div class="form-actions"><i class="icon-home "></i> COMPANY DETAILS</div></th>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">COMPANY NAME:</th>
        <td align="left" valign="top"><input name="company" type="text" value="" size="120" maxlength="120" autocomplete="off" required/></td>
        <th width="16%" align="left" valign="middle" scope="row">POSTCODE:</th>
        <td width="44%" align="left"><?php
 $q1 = "SELECT distinct postcode FROM `postcode` ORDER BY postcode";
$result_post = mysql_query ($q1) or die("ERROR GETTING DATA. REASON: " . mysql_error());
?>
          <select name="postcode" id="postcode" required>
            <option value=""></option>
            <?php
while ($row = mysql_fetch_array($result_post)) {
 ?>
            <option value="<?php echo $row['postcode']; ?>"><?php echo $row['postcode'];?></option>
            <?php }
 ?>
          </select></td>
        </tr>
            <tr>
              <th align="left" valign="middle" scope="row">ZONE:</th>
              <td align="left"><?php $q = "SELECT * FROM `zmast` where status = '1' ORDER BY zone";
$results = mysql_query ($q) or die("ERROR GETTING DATA. REASON: " . mysql_error());
?>
                <select name="zone">
                  <?php
while ($row = mysql_fetch_array($results)) {
 ?>
                  <option value="<?php echo $row['id']; ?>" selected="selected"><?php echo( htmlspecialchars( $row['zone'] ) );?> - <?php echo( htmlspecialchars( $row['zone_desc'] ) );?></option>
                  <?php }
 ?>
                </select></td>
              <th align="left" valign="middle" scope="row">AREA:</th>
              <td align="left"><select name="sub_menu_id" id="sub_menu_id" required>
                <option value=""></option>
                <option value="<?php echo $row['area']; ?>"><?php echo $row['area'];?></option>
              </select></td>
              </tr>
            <tr>
        <th align="left" valign="middle" scope="row">LOT/NO:</th>
        <td align="left"><input type="text"  name="lot" value=""  maxlength="64"  autocomplete="off" width="64" required/></td>
        <th align="left" valign="middle" scope="row">OFFICE NO:</th>
        <td align="left"><input name="office" type="text" value="" size="10" maxlength="10" width="10" autocomplete="off" required/>
          <font color="#FF0000" size="1em"> **e.g: 036875000</font></td>
            </tr>
               <tr>
        <th align="left" valign="middle" scope="row">ADDRESS 1:</th>
        <td align="left"><input name="add1" type="text" value="" size="120" maxlength="120" autocomplete="off" required/></td>
        <th align="left" valign="middle" scope="row">CUSTOMER LIMIT:</th>
        <td align="left"><?php $q = "SELECT * FROM `limits`  ORDER BY ceiling";
$results = mysql_query ($q) or die("ERROR GETTING DATA. REASON: " . mysql_error());
?>
          <select name="lmt">
            <?php
while ($row = mysql_fetch_array($results)) {
 ?>
            <option value="<?php echo $row['id']; ?>">RM <?php echo( htmlspecialchars( numberfix($row['ceiling'] )) );?></option>
            <?php }
 ?>
          </select></td>
               </tr>
               <tr>
        <th align="left" valign="middle" scope="row">ADDRESS 2:</th>
        <td align="left"><input name="add2" type="text" value="" size="120" maxlength="120" autocomplete="off" required/></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
               <tr>
                 <th align="left" valign="middle" scope="row">ADDRESS 3:</th>
                 <td align="left"><input name="add3" type="text" value="" size="120" maxlength="120" autocomplete="off" /></td>
                 <td align="left">&nbsp;</td>
                 <td align="left">&nbsp;</td>
               </tr>
               <tr><th colspan="4" align="left" valign="middle" scope="row"><div class="form-actions"><i class="icon-user "></i> CONTACT DETAILS</div></th>
               </tr>
        <tr>
          <th width="12%" align="left" valign="middle" scope="col">SALUTATION:</th>
          <th width="28%" align="left" scope="col"><select name="salut" required>
            <option value=""></option>
            <option value="MR">MR</option>
            <option value="MRS">MRS</option>
            <option value="MDM">MDM</option>
            <option value="ENCIK">ENCIK</option>
            <option value="PUAN">PUAN</option>
            <option value="CIK">CIK</option>
            <option value="DATUK">DATUK</option>
            <option value="YB">YB</option>
            <option value="DATO SRI">DATO SRI</option>
            <option value="PUAN SRI">PUAN SRI</option>
            <option value="TAN SRI">TAN SRI</option>
          </select></th>
          <th align="left" valign="middle" scope="row">ID NO:</th>
          <td align="left"><input name="idnum" type="text" value="" size="12" maxlength="12" width="10" autocomplete="off"/>
            <font color="#FF0000" size="1em"> **e.g: 801120091726</font></td>
          </tr>
      <tr>
        <th align="left" valign="middle" scope="row">NAME:</th>
        <td align="left"><input name="name" type="text" value="" size="64" maxlength="64" autocomplete="off" required/></td>
        <th align="left" valign="middle" scope="row">MOBILE NO:</th>
        <td align="left"><input name="mobile" type="text" value="" size="10" maxlength="10" width="10" autocomplete="off" required/>
          <font color="#FF0000" size="1em"> **e.g: 0172080769</font></td>
        </tr>
      <tr>
        <th align="left" valign="middle" scope="row">E-MAIL:</th>
        <td align="left"><input name="mail" type="text" value="" size="40" autocomplete="off" required/></td>
        <th align="left" valign="middle" scope="row">PHOTO:</th>
        <td align="left"><input type="hidden" name="MAX_FILE_SIZE" value="99999999999999" />
          <input name="userfile" type="file" id="userfile" size="30" /></td>
        </tr>
      <tr>
        <th align="left" valign="middle" scope="row">DESIGNATION:</th>
        <td align="left"><select name="post" required>
          <option value=""></option>
          <option value="DIRECTOR">DIRECTOR</option>
          <option value="MANAGER">MANAGER</option>
          <option value="ASSISTANT MANAGER">ASSISTANT MANAGER</option>
          <option value="SALES">SALES</option>
          <option value="BUSSINES OWNER">BUSSINES OWNER</option>
          <option value="SUPERVISOR">SUPERVISOR</option>
          <option value="TECHNICIAN">TECHNICIAN</option>
          <option value="STAFF">STAFF</option>
          <option value="OTHERS">OTHERS</option>
        </select></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">ID TYPE:</th>
        <td align="left"><select name="idtype" required>
          <option value=""></option>
          <option value="STAFF ID">STAFF ID</option>
          <option value="I/C">I/C</option>
          <option value="PASSPORT">PASSPORT</option>
          <option value="BC">BIRTH CERTIFICATE</option>
          </select></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      </table>
 </div>
 <div class="form-actions">

  <input name="button" type="submit" class="btn btn-small" value="Create   " id="submitButton0"> 
   <input name="button" class="btn btn-small" type="reset"   value="Reset    ">   
   <a href="cus_edit.php" role="button" class="btn btn-small">Back</a>
   
										</div> <!-- /form-actions -->
									</fieldset>
<?php

	 break;
    case "Edit  ":
$id = "";
$id = $_REQUEST['id'];
//echo $id;

$q = " SELECT a.id, a.company_name, a.salutation, a.name, a.user_email, a.post, a.office_no, a.mobile_no, a.id_type, a.id_no, a.lot, a.address_1, a.address_2, a.address_3, a.postcode, a.area, d.state_name, a.image, a.image_type, a.image_size, a.date_added, a.user, b.zone, b.zone_desc, c.status_desc, e.ceiling, a.limit_id, a.zone_id
						  FROM cfmast a 
			 			  JOIN zmast b ON b.id = a.zone_id 
						  LEFT JOIN statmast c ON c.id = a.status_id
						  LEFT JOIN limits e ON e.id = a.limit_id
						  LEFT JOIN state d ON d.state_code = a.state_code WHERE a.id = ".$id;
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($r=mysql_fetch_array($results)) {
$salut = $r['salutation'];
$name = $r['name'];
$mail = $r['user_email'];
$post = $r['post'];
$idtype = $r['id_type'];
$idnum = $r['id_no'];
$office = $r['office_no'];
$mobile = $r['mobile_no'];
$image = $r['image'];
$img_type = $r['image_type'];
$size = $r['image_size'];
$date = $r['date_added'];
$lmt = $r['ceiling'];
$lmtid = $r['limit_id'];
$zneid = $r['zone_id'];
$zne = $r['zone'];
$znedesc = $r['zone_desc'];
?>
<h5>Maintenance: Edit</h5>
 <div class="accordion-inner">
 </div>
 
 <input name="id" type="hidden" value="<?php echo $id;?>">
 <table width="91%" border="0" cellpadding="0" cellspacing="2">
      <tr>
        <th colspan="4" align="left" valign="middle" scope="row"> <div class="form-actions"><i class="icon-home "></i> COMPANY DETAILS</div></th>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">COMPANY NAME:</th>
        <td align="left" valign="top"><input name="company" type="text" value="<?php echo( htmlspecialchars( $r['company_name'] ) ); ?>" size="120" maxlength="120" autocomplete="off" required/></td>
        <th width="14%" align="left" valign="middle" scope="row">POSTCODE:</th>
        <td width="48%" align="left"><?php
 $q1 = "SELECT distinct postcode FROM `postcode` ORDER BY postcode";
$result_post = mysql_query ($q1) or die("ERROR GETTING DATA. REASON: " . mysql_error());
?>
          <select name="postcode" id="postcode" required>
            <option value="<?php echo $r['postcode']; ?>" selected="selected"><?php echo( htmlspecialchars( $r['postcode'] ) ); ?></option>
            <?php
while ($row = mysql_fetch_array($result_post)) {
 ?>
            <option value="<?php echo $row['postcode']; ?>"><?php echo $row['postcode'];?></option>
            <?php }
 ?>
          </select></td>
        </tr>
            <tr>
              <th align="left" valign="middle" scope="row">ZONE:</th>
              <td align="left"><?php $q = "SELECT * FROM `zmast` where status = '1' ORDER BY zone";
$results = mysql_query ($q) or die("ERROR GETTING DATA. REASON: " . mysql_error());
?>
                <select name="zone">
 <option value="<?php echo $zneid; ?>" selected="selected"><?php echo( htmlspecialchars( $zne ) );?> - <?php echo( htmlspecialchars( $znedesc ) );?></option>
                  <?php
while ($row = mysql_fetch_array($results)) {
 ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo( htmlspecialchars( $row['zone'] ) );?> - <?php echo( htmlspecialchars( $row['zone_desc'] ) );?></option>
                  <?php }
 ?>
                </select></td>
              <th align="left" valign="middle" scope="row">AREA:</th>
              <td align="left"><select name="sub_menu_id" id="sub_menu_id" required>
                <option value="<?php echo $r['area']; ?>" selected="selected"><?php echo( htmlspecialchars( $r['area'] ) ); ?></option>
                <option value="<?php echo $row['area']; ?>"><?php echo $row['area'];?></option>
              </select></td>
              </tr>
            <tr>
        <th align="left" valign="middle" scope="row">LOT/NO:</th>
        <td align="left"><input type="text"  name="lot" value="<?php echo( htmlspecialchars( $r['lot'] ) ); ?>"  maxlength="64"  autocomplete="off" width="64" required/></td>
        <th align="left" valign="middle" scope="row">OFFICE NO:</th>
        <td align="left"><input name="office" type="text" value="<?php echo( htmlspecialchars( $office ) ); ?>"  class=""  id="<?php $office;?>" size="10" maxlength="10" width="10" autocomplete="off" required/>
          <font color="#FF0000" size="1em"> **e.g: 036875000</font></td>
            </tr>
               <tr>
        <th align="left" valign="middle" scope="row">ADDRESS 1:</th>
        <td align="left"><input name="add1" type="text" value="<?php echo( htmlspecialchars( $r['address_1'] ) ); ?>" size="120" maxlength="120" autocomplete="off" required/></td>
        <th align="left" valign="middle" scope="row">CUSTOMER LIMIT:</th>
        <td align="left"><?php $q = "SELECT * FROM `limits`  ORDER BY ceiling";
$results = mysql_query ($q) or die("ERROR GETTING DATA. REASON: " . mysql_error());
?>
          <select name="lmt">
          <option value="<?php echo $lmtid; ?>" selected="selected">RM <?php echo( htmlspecialchars( numberfix($lmt )) );?></option>
            <?php
while ($row = mysql_fetch_array($results)) {
 ?>
            <option value="<?php echo $row['id']; ?>">RM <?php echo( htmlspecialchars( numberfix($row['ceiling'] )) );?></option>
            <?php }
 ?>
          </select></td>
               </tr>
               <tr>
        <th align="left" valign="middle" scope="row">ADDRESS 2:</th>
        <td align="left"><input name="add2" type="text" value="<?php echo( htmlspecialchars( $r['address_2'] ) ); ?>" size="120" maxlength="120" autocomplete="off" required/></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
               <tr>
        <th align="left" valign="middle" scope="row">ADDRESS 3:</th>
        <td align="left"><input name="add3" type="text" value="<?php echo( htmlspecialchars( $r['address_3'] ) ); ?>" size="120" maxlength="120" autocomplete="off" /></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
               <tr>
        <th align="left" valign="middle" scope="row">&nbsp;</th>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
               <tr>
                 <th align="left" valign="middle" scope="row">&nbsp;</th>
                 <td align="left">&nbsp;</td>
                 <td align="left">&nbsp;</td>
                 <td align="left">&nbsp;</td>
               </tr>
               <tr>
        <th align="left" valign="middle" scope="row">&nbsp;</th>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr><th colspan="4" align="left" valign="middle" scope="row"><div class="form-actions"><i class="icon-user "></i> CONTACT DETAILS</div></th>
      </tr>
        <tr>
          <th colspan="4" align="left" valign="middle" scope="col"> <?php echo '<img width="86" height="86" src="data:'.$img_type.';base64,' . base64_encode( $image ) . '" />';?> <br> <a href="#myModal" role="button" class="btn-warning" data-toggle="modal"><input name="button" type="submit" class=" btn btn-warning" value="Change" id="<?php echo $id;?>"></a></th>
          </tr>
        <tr>
          <th width="11%" align="left" valign="middle" scope="col">SALUTATION:</th>
          <th width="27%" align="left" scope="col"><select name="salut" required>
            <option value="<?php echo $salut; ?>" selected="selected"><?php echo( htmlspecialchars( $salut ) ); ?></option>
            <option value="MR">MR</option>
            <option value="MRS">MRS</option>
            <option value="MDM">MDM</option>
            <option value="ENCIK">ENCIK</option>
            <option value="PUAN">PUAN</option>
            <option value="CIK">CIK</option>
            <option value="DATUK">DATUK</option>
            <option value="YB">YB</option>
            <option value="DATO SRI">DATO SRI</option>
            <option value="PUAN SRI">PUAN SRI</option>
            <option value="TAN SRI">TAN SRI</option>
          </select></th>
          <th align="left" valign="middle" scope="row">ID NO:</th>
          <td align="left"><input name="idnum" type="text" value="<?php echo( htmlspecialchars( $idnum ) ); ?>"  class=""  id="<?php $idnum;?>" size="12" maxlength="12" width="10" autocomplete="off"/>
            <font color="#FF0000" size="1em"> **e.g: 801120091726</font></td>
        </tr>
      <tr>
        <th align="left" valign="middle" scope="row">NAME:</th>
        <td align="left"><input name="name" type="text" value="<?php echo( htmlspecialchars( $name ) ); ?>"  class=""  id="<?php $name;?>"  size="64" maxlength="64" autocomplete="off" required/></td>
        <th align="left" valign="middle" scope="row">MOBILE NO:</th>
        <td align="left"><input name="mobile" type="text" value="<?php echo( htmlspecialchars( $mobile ) ); ?>"  class=""  id="<?php $mobile;?>" size="10" maxlength="10" width="10" autocomplete="off" required/>
          <font color="#FF0000" size="1em"> **e.g: 0172080769</font></td>
        </tr>
      <tr>
        <th align="left" valign="middle" scope="row">E-MAIL:</th>
        <td align="left"><input name="mail" type="text" value="<?php echo( htmlspecialchars( $mail ) ); ?>"  class=""  id="<?php $mail;?>" size="40" autocomplete="off" required/></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">DESIGNATION:</th>
        <td align="left"><select name="post" required>
          <option value="<?php echo $post; ?>" selected="selected"><?php echo( htmlspecialchars( $post ) ); ?></option>
          <option value="DIRECTOR">DIRECTOR</option>
          <option value="MANAGER">MANAGER</option>
          <option value="ASSISTANT MANAGER">ASSISTANT MANAGER</option>
          <option value="SALES">SALES</option>
          <option value="BUSSINES OWNER">BUSSINES OWNER</option>
          <option value="SUPERVISOR">SUPERVISOR</option>
          <option value="TECHNICIAN">TECHNICIAN</option>
          <option value="STAFF">STAFF</option>
          <option value="OTHERS">OTHERS</option>
        </select></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">ID TYPE:</th>
        <td align="left"><select name="idtype" required>
          <option value="<?php echo $idtype; ?>" selected="selected"><?php echo( htmlspecialchars( $idtype ) ); ?></option>
          <option value="StaffID">STAFF ID</option>
          <option value="I/C">I/C</option>
          <option value="PASSPORT">PASSPORT</option>
          <option value="BC">BIRTH CERTIFICATE</option>
          </select></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      </table>
<p></p>

 <div class="form-actions">

  <input name="button" type="submit" class="btn btn-small" value="Save   " id="submitButton0">  
   <a href="cus_edit.php" role="button" class="btn btn-small">Back</a>
   
										</div> <!-- /form-actions -->
									</fieldset>
									
 <!--<a href="#myModal" input name="button" role="button" class="btn btn-small" data-toggle="modal"disabled id="submitButton2">Delete</a>-->
 <!--<input name="button" class="btn btn-small" type="reset"   value="Reset    ">-->  
  <!-- Modal -->
                                                    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                      <div class="modal-header">
                                                        <h3 id="myModalLabel">UPDATE CONTACT PHOTO</h3>
                                                      </div>
                                                      <div class="modal-body">
                                                        <p><form action="cus_process_submit.php" method="post" enctype="multipart/form-data">
														<th align="left" valign="middle" scope="row">NEW PHOTO: </th>
														<td align="left"><input type="hidden" name="MAX_FILE_SIZE" value="99999999999999" />
														<input name="userfile" type="file" id="userfile" size="30" /></td>
														<input name="id" type="hidden" value="<?php echo $id;?>">
														</form></p>
                                                      </div>
                                                      <div class="modal-footer">
                                                         <input name="button" class="btn btn-primary"type="submit" class="btn btn-small" value="OK">
                                                        <button class="btn btn-small" data-dismiss="modal" aria-hidden="true">NO</button>
                                                        
                                                      </div>
                                                    </div>
<?php
}
	 break;
    case "OK":
?>
  </h5> Maintenance: Delete</h5>
 <div class="accordion-inner">
<?php
 
        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');


$data = $_REQUEST['id'];


$sql = "DELETE FROM `cfmast` where id = '$data'";
$ok=mysql_query($sql) or die("ERROR GETTING DATA. REASON: " . mysql_error());
		   if ($ok)
		   {
		   echo "Record deleted";
		   }
		else 
		   {
		   echo "<font color = 'red' size = '3'>WARNING:</font> Unable to remove record. Please contact system administrator";
		   }
unset($_POST);
header("Location:customer.php");
?>

  
 </div>
 <div class="form-actions">
   
   <a href="zone_edit.php" role="button" class="btn btn-small">Back</a>
   
										</div> <!-- /form-actions -->
									</fieldset>
<?php
$id = "";
$id = $_POST['id'];
    break;
}//switch
}//close else if
}//close isset

?>

										
<div class="controls">
</div>	<!-- /controls -->			
</div> <!-- /control-group -->

                                        
										
											
										 <br />
										
											
										
								</form>
                                                          </div>
                                                        </div>	
      		

								</div>
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script src="js/1.8.3/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
$("#postcode").bind("change", function() {
$.ajax({
type: "GET",
url: "get_sub_category.php",
data: "postcode="+$("#postcode").val(),
success: function(html) {
$("#sub_menu_id").html(html);
}
});
});
});
</script>
  </body>

</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>