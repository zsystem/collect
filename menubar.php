<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		
		    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a><div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --></div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
	       <ul class="mainnav">        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>
        <li><a href="reports.html"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
      		            <li><a href="zone.php">Zone</a></li>
            <li><a href="cycle.php">Cycle Code</a></li>
            <li><a href="limit.php">Customer Limit</a></li>
            <li><a href="status.php">Status</a></li>
            <li><a href="severity.php">Feedback Severity</a></li>
			<li><a href="feedback.php">Feedback Status</a></li>

          </ul>          
			<!-- <li class="active"><a href="#"><i class="icon-user "></i><span>Customers</span> </a></li>    -->
			<!-- <li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li>          -->  

			<?php } ?> <?php if ( ($session->isMaster()) || ($session->isAgent())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>	
		 <li><a href="workcard.php"><i class=" icon-edit"></i><span>Work Card</span></a></li><li><a href="contract.php"><i class="icon-th-large"></i><span>Contract</span> </a></li>
        <li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
        <li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    
    </div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    