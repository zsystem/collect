<?php

include('script_gb.php');

//*** testing PDO ***//
include('dbcon.php');?>
<table width="100%" border="1" cellpadding="2">
  <tr>
    <td bgcolor="#0099FF"><font color="#000000"><strong>1. Update Action</strong></font></td>
    <td><font color="#000000">2. Confirm Action</font></td>
    <td><font color="#000000">3. Record Updated</font></td>
  </tr>
</table>
<p></p>

<?php
$id = $_GET['id'];
try {
$stmt = $dbh->prepare('
SELECT 
distinct
a.contract_id, d.severity, h.company_name, g.accno, c.unit_price, c.tax_amount, c.gross_total, (sum(c.unit_price+c.tax_amount)) as TOTAL, a.remarks, i.MIA
FROM workcard a
JOIN workmast b on b.id = a.workmast_id
JOIN invoice c on c.contract_id = a.contract_id
LEFT JOIN severity d on d.id = c.status
LEFT JOIN payment e on e.invoice_num = c.invoice_num
LEFT JOIN payment_hist f on f.invoice_num = c.invoice_num
LEFT JOIN contract g on g.contract_id = a.contract_id
LEFT JOIN cfmast h on h.id = g.customer_id
LEFT JOIN mia i on i.contract_id = a.contract_id
WHERE a.card = 1
AND c.contract_id = "'.$id.'"
AND d.id not in ("20")
group by a.contract_id, d.severity, g.accno, c.unit_price, c.tax_amount, c.gross_total, a.remarks, i.MIA
ORDER BY h.company_name');
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach ($result as $row){?>

<form name="myForm" action="w_active_process.php">
  <table width="100%" border="0">
    <tr>
      <td width="13%" valign="middle"><strong>CONTRACT ID:</strong></td>
      <td width="21%" valign="middle"><input name="contractID" value="<?php echo( htmlspecialchars( $row['contract_id'] ) )?>" readonly/></td>
      <td width="12%" valign="middle"><strong>ACTION DATE:</strong></td>
      <td width="24%" valign="middle"><input id="datepicker-example2" type="text" name="miscdate" required/></td>
    </tr>
    <tr>
      <td valign="middle"><strong>ACCOUNT NO:</strong></td>
      <td valign="middle"><input name="accno" value="<?php echo( htmlspecialchars( $row['accno'] ) )?>" readonly/></td>
      <td valign="middle"><strong>WITH REBATE:</strong></td>
      <td valign="middle"><select name="rebate" id="rebate" onchange="enableTextbox2()">
        <option value='N' selected="selected">NO</option>
        <option value='Y'>YES</option>
      </select></td>
    </tr>
    <tr>
      <td valign="middle"><strong>TENANT:</strong></td>
      <td valign="middle"><input name="company_name" value="<?php echo( htmlspecialchars( $row['company_name'] ) )?>" readonly/></td>
      <td valign="middle"><strong>REBATE:</strong></td>
    <td valign="middle"><input name="disc" id="disc" type="text" class="auto"  data-a-sign="" disabled="disabled"/>      </tr>
    <tr>
      <td valign="middle"><strong>MONTHS IN ARREARS:</strong></td>
	  <td valign="middle"><input name="MIA" value="<?php echo( htmlspecialchars( $row['MIA'] ) )?>" readonly/></td>	
      <td valign="middle"><strong>REFERENCE NO:</strong></td>
      <td valign="middle"><input name="ref" id="ref" type="text" disabled="disabled"/></td>
    </tr>
    <tr>
      <td valign="middle"><strong>ACTION TYPE:</strong></td>
      <td valign="middle">
           <?php  //populate action type
	 		$prep = $dbh->prepare('select id, status, severity_id from feedback order by severity_id');
	 		$prep->execute();
			$result = $prep->fetchAll(PDO::FETCH_ASSOC);
			?>
      <select name="action" id="action" onchange="enableTextbox()" required>
      <option value=0></option>
        <?php
foreach ($result as $r){ 
 ?>
        <option value="<?php echo $r['id']; ?>"><?php echo( htmlspecialchars( $r['status'] ) );?></option>
        <?php }
 ?>
      </select></td>
      <td valign="middle"><strong>PAYMENT AMOUNT:</strong></td>
      <td valign="middle" ><input name="pay" id="pay" type="text" class="auto"  data-a-sign="" disabled="disabled"/></td>
    </tr>
    <tr>
      <td valign="middle"><strong>NEXT ACTION:</strong></td>
      <td valign="middle"><?php  //populate action type
	 		$prep = $dbh->prepare('select * from severity order by severity');
	 		$prep->execute();
			$result = $prep->fetchAll(PDO::FETCH_ASSOC);
			?>
        <select name="sev" id="sev" required>
          <?php
foreach ($result as $r){ 
 ?>
          <option value="<?php echo $r['id']; ?>"><?php echo( htmlspecialchars( $r['severity'] ) );?></option>
          <?php }
 ?>
      </select></td>
      <td valign="middle" ><strong>UNPAID INVOICE</strong>:</td>
      <td valign="middle" ><?php 
			$inv = $dbh->prepare('select count(*) as OS from invoice where contract_id = "'.$id.'"');
	 		$inv->execute();
			$result = $inv->fetchAll(PDO::FETCH_ASSOC);	  
			foreach ($result as $r){ 
			?><a href="#myModal" role="button" class="btn-warning" data-toggle="modal"><input name="button" type="submit" class="btn-danger" value="<?php echo( htmlspecialchars( $r['OS'] ) ); ?> Invoice(s)" /></a>
			<?php }?>
            </td>
    </tr>
    <tr>
      <td valign="top"><strong>REMARKS:</strong></td>
      <td colspan="3" valign="middle"><textarea name="rem" cols="100" rows="10" id="rem"></textarea></td>
    </tr>
  </table>
<div class="form-actions">
<input name="button" type="submit" class="btn btn-small" value="Update">  
   <!-- Button to trigger modal--> 
<input name="button" type="submit" class=" btn btn-small" value="History">
 </div> 
  <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                                        <h4 id="myModalLabel">MAINTENANCE HISTORY</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        <p>Deleted record cannot be recovered anymore. Do you wish to proceed?</p>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-small" data-dismiss="modal" aria-hidden="true">NO</button>
                                                        
                                                      </div>
  </div>
										</div>   
</form>
            <?php 	$dbh = null;

}} catch(PDOException $ex) {
 
    echo $ex->getMessage();
}?>
</table>

<script type="text/javascript" src="jquery-1.7.2.js"></script>
<script type="text/javascript" src="views/javascript/zebra_pagination.js"></script>
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script src="js/1.8.3/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="js/dropdown.js"></script>


<script type = "text/javascript">
function enableTextbox() {
var val = document.getElementById("action").selectedIndex;
var op = document.getElementById("action").options;
if (op[val].value == '11') { document.getElementById("pay").disabled = false}
else { document.getElementById("pay").disabled = true}
if (op[val].value == '11') { document.getElementById("rebate").disabled = false}
else { document.getElementById("rebate").disabled = true}
}

</script>
<script type = "text/javascript">
function enableTextbox2() {
var val = document.getElementById("rebate").selectedIndex;
var op = document.getElementById("rebate").options;
if (op[val].value =='Y') { document.getElementById("disc").disabled = false}
else { document.getElementById("disc").disabled = true}
if (op[val].value =='Y') { document.getElementById("ref").disabled = false}
else { document.getElementById("ref").disabled = true}
}

</script>
<script type="text/javascript" src="js/autoNumeric.js"></script>
<script type="text/javascript">
jQuery(function($) {
$('.auto').autoNumeric('init');
});
</script>

	      			
      		