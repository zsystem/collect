<?php
error_reporting(0);
/*** http://wiki.hashphp.org/PDO_Tutorial_for_MySQL_Developers ***/

/*** mysql hostname ***/
$hostname = 'localhost';

/*** mysql username ***/
$username = 'root';

/*** mysql password ***/
$password = '';

try {
    $dbh = new PDO("mysql:host=$hostname;dbname=collect;charset=utf8", $username, $password);
	
	/*** set PDO attribute ***/
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    
	/*** echo a message saying we have connected ***/
    /*** echo 'Connected to database'; ***/
    }
catch(PDOException $e)
    {
    echo $e->getMessage();
    }
?>