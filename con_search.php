<?php
// checking for minimum PHP version
include("include/classes/session.php");
include("include/connection.php");
if (($session->logged_in) == true) {


?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Contract - Collect+</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="css/googleapis.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="views/reset.css" type="text/css">
    <link rel="stylesheet" href="views/style.css" type="text/css">
    <link rel="stylesheet" href="views/zebra_pagination.css" type="text/css">
    <link href="css/datepicker.css" rel="stylesheet">
     <link href="css/prettify.css" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 <style type="text/css">
<!--
.a {	color: #FFF;
} 
 body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
}

	
-->
  </style>
  <style type="text/css">
/* These classes are used by the script as rollover effect for table 1 and 2 */
	
	.tableRollOverEffect1{
		background-color:#317082;
		color:#FFF;
	}

	.tableRollOverEffect2{
		background-color:#000;
		color:#FFF;
	}
	
	.tableRowClickEffect1{
		background-color:#F00;
		color:#FFF;
	}
	.tableRowClickEffect2{
		background-color:#00F;
		color:#FFF;
	}
.countries {
	color: #000000;
}


	#alert {
		display: none;
	}

  </style>
<link href="css/prettify.css" rel="stylesheet">
<script src="sorttable.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var arrayOfRolloverClasses = new Array();
	var arrayOfClickClasses = new Array();
	var activeRow = false;
	var activeRowClickArray = new Array();
	
	function highlightTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(this!=activeRow){
			this.setAttribute('origCl',this.className);
			this.origCl = this.className;
		}
		this.className = arrayOfRolloverClasses[tableObj.id];
		
		activeRow = this;
		
	}
	
	function clickOnTableRow()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;		
		
		if(activeRowClickArray[tableObj.id] && this!=activeRowClickArray[tableObj.id]){
			activeRowClickArray[tableObj.id].className='';
		}
		this.className = arrayOfClickClasses[tableObj.id];
		
		activeRowClickArray[tableObj.id] = this;
				
	}
	
	function resetRowStyle()
	{
		var tableObj = this.parentNode;
		if(tableObj.tagName!='TABLE')tableObj = tableObj.parentNode;

		if(activeRowClickArray[tableObj.id] && this==activeRowClickArray[tableObj.id]){
			this.className = arrayOfClickClasses[tableObj.id];
			return;	
		}
		
		var origCl = this.getAttribute('origCl');
		if(!origCl)origCl = this.origCl;
		this.className=origCl;
		
	}
		
	function addTableRolloverEffect(tableId,whichClass,whichClassOnClick)
	{
		arrayOfRolloverClasses[tableId] = whichClass;
		arrayOfClickClasses[tableId] = whichClassOnClick;
		
		var tableObj = document.getElementById(tableId);
		var tBody = tableObj.getElementsByTagName('TBODY');
		if(tBody){
			var rows = tBody[0].getElementsByTagName('TR');
		}else{
			var rows = tableObj.getElementsByTagName('TR');
		}
		for(var no=0;no<rows.length;no++){
			rows[no].onmouseover = highlightTableRow;
			rows[no].onmouseout = resetRowStyle;
			
			if(whichClassOnClick){
				rows[no].onclick = clickOnTableRow;	
			}
		}
		
	}
</script>
<script type="text/javascript" src="x.js"></script> 
<script type="text/javascript"> 
function adjustLayout()
{
  // Get natural heights
  var cHeight = xHeight("contentcontent");
  var lHeight = xHeight("leftcontent");
  var rHeight = xHeight("rightcontent");
 
  // Find the maximum height
  var maxHeight =
    Math.max(cHeight, Math.max(lHeight, rHeight));
 
  // Assign maximum height to all columns
  xHeight("content", maxHeight);
  xHeight("left", maxHeight);
  xHeight("right", maxHeight);
 
  // Show the footer
  xShow("footer");
}
 
window.onload = function()
{
  xAddEventListener(window, "resize",
    adjustLayout, false);
  adjustLayout();
}
</script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/autoNumeric.js"></script>
    <script type="text/javascript">
jQuery(function($) {
    $('.auto').autoNumeric('init');
});
</script>
</head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		
		    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a><a class="brand" href="index.php">Collect+ </a>     <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class=""><a href="#" class="dropdown" data-toggle="dropdown"><i
                            class="icon-info-sign"></i> <?php 
        // if could not connect to database

		  echo "<strong>Branch: </strong>".$branch." - ".$branch_desc; ?> <b class=""></b></a>
            <ul class="">

            </ul>
          </li>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-off"></i> &nbsp;&nbsp;<?php 
        // if could not connect to database

		  echo $session->username." - ".$realname; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                          <li><?php echo "<a href=\"userinfo.php?user=$session->username\">My Account</a>"; ?> </li>
              <li><a href="useredit.php">Profile</a></li>
              <li><a href="process.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> </div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php if (($session->logged_in) && ($session->isAdmin())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span></a></li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file-alt"></i><span>Parameters</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
      		            <li><a href="zone.php">Zone</a></li>
            <li><a href="cycle.php">Period Code</a></li>
            <li><a href="limit.php">Customer Limit</a></li>
            <li><a href="status.php">Status</a></li>
            <li><a href="severity.php">Feedback Severity</a></li>
			<li><a href="feedback.php">Feedback Status</a></li>


          </ul>
          <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?> <?php if ( ($session->isMaster()) || ($session->isAgent())) {?>
        <li><a href="index.php"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="reports.php"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>	
		 <li><a href="workcard.php"><i class=" icon-edit"></i><span>Work Card</span></a></li><li class="active"><a href="contract.php"><i class="icon-th-large"></i><span>Contract</span> </a></li>
        <!--<li><a href="shortcodes.html"><i class="icon-file-alt"></i><span>Parameter Maintenance</span> </a> </li>-->
<li><a href="customer.php"><i class="icon-user "></i><span>Customers</span> </a></li> 
<li><a href="property.php"><i class="icon-home"></i><span>Property</span> </a></li><?php } ?>
    </div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pushpin"></i>
	      				<h3>Customer Contract</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
                        <li ><a href="contract.php">Inquiry</a></li>
						  <li class="active">
						    <a href="#formcontrols" data-toggle="tab">Maintenance</a>
						  </li>

						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane" id="formcontrols">

								</div>
								
								<div class="tab-pane active" id="jscontrols">

<div class="control-group">											

	      			
 
	          <?php


$key = $_REQUEST['co'];


$mode = "";

$mode = $_REQUEST['button'];


if(isset($_POST['button'])) {
if (empty($mode)) {
echo 'invalid';
}//close if empty mode
else if ($mode = $_REQUEST['button']) { 
switch ($mode) {
    case "Search": 
?>
<h5>Maintenance: Add</h5>
 <div class="accordion-inner">
                                                          
                                                          <form action="con_search.php" method="post">
                                                          COMPANY NAME:  <input name="co" type="text" required>
                                                          </select>
                                                          <input name="button" type="submit" class=" btn-info" value="Search" id="submitButton0"> 
                                                          </form>
                                                          </div></div>

 <form action="con_process.php" method="post" enctype="multipart/form-data"> 
<div class="control-group">											
<?php

        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

        // how many records should be displayed on a page?
        $records_per_page = 20;

        // include the pagination class
        require 'Zebra_Pagination.php';

        // instantiate the pagination object
        $pagination = new Zebra_Pagination();

        // set position of the next/previous page links
        $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

        // the MySQL statement to fetch the rows
        // note how we build the LIMIT
        // also, note the "SQL_CALC_FOUND_ROWS"
        // this is to get the number of rows that would've been returned if there was no LIMIT
        // see http://dev.mysql.com/doc/refman/5.0/en/information-functions.html#function_found-rows
        $MySQL = '
            SELECT
                SQL_CALC_FOUND_ROWS
                *
            FROM
                cfmast WHERE company_name LIKE "%'.$key.'%"  
            ORDER BY
                id 
            LIMIT
                ' . (($pagination->get_page() - 1) * $records_per_page) . ', ' . $records_per_page . '
        ';


        // if query could not be executed
        if (!($result = @mysql_query($MySQL)))

            // stop execution and display error message
            die(mysql_error());

        // fetch the total number of records in the table
        $rows = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows'));

        // pass the total number of records to the pagination class
        $pagination->records($rows['rows']);

        // records per page
        $pagination->records_per_page($records_per_page);

        ?>            <table border="1" class="table" name="target-1">
<thead>
        	<tr>
            <th width="2%" bgcolor="#333" class="widget-table">Select</th>
             <th width="20%" bgcolor="#333">Company Name</th>
            <th width="20%" bgcolor="#333">Contact Person</th>
            <th width="20%" bgcolor="#333">E-Mail</th>
 			<th width="20%" bgcolor="#333">Office Number</th>            </tr>
</thead>
            <?php $index = 0?>

            <?php while ($row = mysql_fetch_assoc($result)):?>

            <tr<?php echo $index++ % 2 ? ' class="even"' : ''?>>
                <td><input name="id" type="radio" value="<?php echo $row["id"];?>"/></td>
                 <td><?php echo $row['company_name']?></a></td>
                 <td><?php echo $row['salutation'].' '.$row['name']?></a></td>
                 <td><?php echo $row['user_email']?></a></td>
                 <td><?php echo $row['office_no']?></a></td>
            </tr>

            <?php endwhile?>
        </table>

        <script type="text/javascript" src="jquery-1.7.2.js"></script>
          <script type="text/javascript" src="views/javascript/zebra_pagination.js"></script>
	      			
  <div class="accordion-inner">
	          <?php

        // render the pagination links
        $pagination->render();

        ?>
        <div class="form-actions">
                                        <script type="text/javascript">
    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton1').removeAttr('disabled');
                }
            else {
                $('#submitButton1').attr('disabled', 'disabled');
                }
            });
        });
	    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton2').removeAttr('disabled');
                }
            else {
                $('#submitButton2').attr('disabled', 'disabled');
                }
            });
        });
			    $(document).ready(function() {
     $('input:radio').click(function() {
            var buttonsChecked = $('input:radio:checked');
            if (buttonsChecked.length) {
                $('#submitButton0').attr('disabled', 'disabled');
                }
            else {
                $('#submitButton0').removeAttr('disabled');
                }
            });
        });
</script>    
  <input name="button" type="submit" class="btn btn-small" value="Proceed  " disabled id="submitButton1"> 
 <a href="con_edit.php" role="button" class="btn btn-small">Back</a>
 </div><!-- /form-actions -->
									</fieldset>
					<?php

    break;
    case "Proceed  ":
	?>
<form action="con_process_submit.php" method="post" enctype="multipart/form-data"> 
     <?php
$id = "";
$ids = $_REQUEST['id'];
?>
<h5>Maintenance: Add</h5>
 <div class="accordion-inner" id="products_modal_box">
 <table width="78%" border="0" cellpadding="1">
      <tr>
        <th width="17%" align="left" valign="middle" scope="row">ZONE:</th>
        <td width="48%" align="left"><?php
 $q1 = "SELECT id, zone, zone_desc FROM `zmast` where status = 1 AND id = '".$branch_id."'";
$result_post = mysql_query ($q1) or die("ERROR GETTING DATA. REASON: " . mysql_error());
?>
            <?php
while ($row = mysql_fetch_array($result_post)) {
 ?>
<input name="zone" id="zone" type="text" value="<?php echo $row['zone'];?> - <?php echo $row['zone_desc']; }?>" size="120" maxlength="120" autocomplete="off" readonly/></td>
        <th width="23%" align="left" scope="col">&nbsp;</th>
        <th width="12%" align="left" scope="col">&nbsp;</th>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">PROPERTY ID:</th>
        <td align="left"><input name="prop" id="prop" type="text" value="" size="120" maxlength="120" autocomplete="off" required/><a href="#myModal" role="button" class="btn btn-info" data-toggle="modal">Search</a></td>
        <td align="left"></td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">COMPANY NAME</th>
        <?php
		$q = " SELECT * from cfmast WHERE id = ".$ids;
$results = mysql_query ($q)or die("ERROR GETTING DATA. REASON: " . mysql_error());
while ($r=mysql_fetch_array($results)) {
$co = $r['company_name'];
$name = $r['name'];
$mail = $r['user_email'];
$post = $r['post'];
$idtype = $r['id_type'];
$idnum = $r['id_no'];
$office = $r['office_no'];
$mobile = $r['mobile_no'];
$image = $r['image'];
$img_type = $r['image_type'];
$size = $r['image_size'];
$date = $r['date_added'];
?>
		
        <td align="left"><input name="company" type="text" value="<?php echo( htmlspecialchars( $co ) ); }?>" size="120" maxlength="120" autocomplete="off" readonly/></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">RENTAL (RM):</th>
        <td align="left"><input name="rent" id="rent" type="text" class="auto"  data-a-sign="" required/></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">ID TYPE:</th>
        <td align="left"><select name="idtype" required>
          <option value=""></option>
          <option value="StaffID">STAFF ID</option>
          <option value="I/C">I/C</option>
          <option value="PASSPORT">PASSPORT</option>
          <option value="BC">BIRTH CERTIFICATE</option>
        </select></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">ID NO:</th>
        <td align="left"><input name="idnum" type="text" value="" size="12" maxlength="12" width="10" autocomplete="off"/>
          <font color="#FF0000" size="1em"> **e.g: 801120091726</font></td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="col">FROM DATE:</th>
        <th align="left" scope="col"> <input class="span2" value="" data-date-format="yyyy-mm-dd" id="dp2" type="text" placeholder="Select Start Date" required></th>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="col">TO DATE:</th>
        <th align="left" scope="col"> <input class="span2" value="" data-date-format="yyyy-mm-dd" id="dp3" type="text" placeholder="Select End Date" required></th>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <th align="left" valign="middle" scope="row">PHOTO:</th>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
    </table>
 </div>
 <div class="form-actions">

  <input name="button" type="submit" class="btn btn-small" value="Create   " id="submitButton0"> 
   <input name="button" class="btn btn-small" type="reset"   value="Reset    ">   
   <a href="contract.php" role="button" class="btn btn-small">Back</a>
   
					</div> <!-- /form-actions -->
<?php
break;
}//switch
}//close else if
}//close isset

?>

										
<div class="controls">

                                                    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h3 id="myModalLabel">PROPERTY SEARCH</h3>
                                                      </div>

                                                        <div class="modal-body">
                                                        <input type="text" id="search" placeholder="Type to search">
  <?php

        // database connection details

        // if could not connect to database
        if (!($connection = @mysql_connect($MySQL_host, $MySQL_username, $MySQL_password)))

            // stop execution and display error message
            die('Error connecting to the database!<br>Make sure you have specified correct values for host, username and password.');

        // if database could not be selected
        if (!@mysql_select_db($MySQL_database, $connection))

            // stop execution and display error message
            die('Error selecting database!<br>Make sure you have specified an existing and accessible database.');

        // how many records should be displayed on a page?
        $records_per_page = 20;

        // include the pagination class
        require 'Zebra_Pagination.php';

        // instantiate the pagination object
        $pagination = new Zebra_Pagination();

        // set position of the next/previous page links
        $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

        // the MySQL statement to fetch the rows
        // note how we build the LIMIT
        // also, note the "SQL_CALC_FOUND_ROWS"
        // this is to get the number of rows that would've been returned if there was no LIMIT
        // see http://dev.mysql.com/doc/refman/5.0/en/information-functions.html#function_found-rows
         $MySQL = '
            SELECT distinct
                SQL_CALC_FOUND_ROWS
a.id, z.zone, a.lot, a.address_1, a.address_2, a.address_3, a.postcode, a.area,  a.date
            FROM
                propmast a 
				left outer join zmast z on z.id = a.zone_id
				WHERE a.zone_id = '.$branch_id.' and contract_id = 0
            ORDER BY
                a.id
            LIMIT
                ' . (($pagination->get_page() - 1) * $records_per_page) . ', ' . $records_per_page . '
        ';


        // if query could not be executed
        if (!($result = @mysql_query($MySQL)))

            // stop execution and display error message
            die(mysql_error());

        // fetch the total number of records in the table
        $rows = mysql_fetch_assoc(mysql_query('SELECT FOUND_ROWS() AS rows'));

        // pass the total number of records to the pagination class
        $pagination->records($rows['rows']);

        // records per page
        $pagination->records_per_page($records_per_page);

        ?>                                                      
                                                        

                                                      </div>
<div class="modal-body">
<p>
<form name="myForm">
<table border="1" class="table" name="myselect" id="table">
    <thead>
        	<tr>
            <th width="2%" bgcolor="#333" class="widget-table">Selection</th>
             <th width="8%" bgcolor="#333">Lot</th>
            <th width="30%" bgcolor="#333">Address 1</th>
            <th width="20%" bgcolor="#333">Address 2</th>
            <th width="12%" bgcolor="#333">Area</th>
            </tr>
    </thead>
            <?php $index = 0?>

            <?php while ($row = mysql_fetch_assoc($result)):1; $re = $row["id"];$lo = $row["lot"];			?>
 <tbody>
            <tr<?php echo $index++ % 2 ? ' class="even"' : ''?>>
                <!--<td><input name="id" type="radio" value="<?php echo $row["id"];?>" onChange="selectItem();"/></td>-->
                               <?php print "<td><a href=\"javascript:;\" onclick=\"$('#prop').val('".$re." - ".$lo."');$('#products_modal_box').dialog('close');\">Choose</a><input type = 'hidden' value= 'prop' id='prop2'></td>";?><!--pass value to parent -->
                 <td><?php echo $row['lot']?></td>
                 <td><?php echo $row['address_1']?></td>
                 <td><?php echo $row['address_2']?></td>
                 <td><?php echo $row['area']?></td>
            </tr>
</tbody>
            <?php endwhile?>
        </table>

        <script type="text/javascript" src="jquery-1.7.2.js"></script>
          <script type="text/javascript" src="views/javascript/zebra_pagination.js"></script>
          	          <?php

        // render the pagination links
        $pagination->render();

        ?>
</p>
                                                      </div>
                                                      <div class="modal-body"><p></p></div>
                                                      <div class="modal-body"><p></p></div>
                                                      <div class="modal-body"><p></p></div>
                                                      <div class="modal-footer">
                                                        
                                                      </div>
                                                    </div>
										</div> <!-- /form-actions -->
</div>	<!-- /controls -->			
</div> <!-- /control-group -->

                                        
										
											
										 <br />
										
									</form></form></form>		
										
								</form>
                              </div>
                          </div>	
      		

					  </div>
								
				  </div>
						  
						  
			  </div>
						
						
						
						
						
			</div> <!-- /widget-content -->
						
		  </div> <!-- /widget -->
	      		
      </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
  </div> <!-- /row -->
	
</div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>

                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">&copy; Collect+ <span class="a">Integrated Collection System. </span></div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
    <script src="js/prettify.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
	<script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'mm-dd-yyyy'
			});
			$('#dp2').datepicker();
			$('#dp3').datepicker();
			$('#dp3').datepicker();
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>
                                                            <script>
														var $rows = $('#table tbody tr');
$('#search').keyup(function () {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function () {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

function selectItem(){
	var select_options=document.myForm.myselect;
	var len=select_options.length;
	var selindex=0;
	for (var i=0;i<len;i++){
		if (select_options[i].checked) {selindex=select_options[i].value;}
	}
	if (selindex!=0) {document.myForm.holder.value=selindex;}
}


														</script>
  </body>

</html>
<?php
} else {
    // the user is not logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are not logged in" view.
    //include("views/not_logged_in.php");
	   include("error.php");
}
?>