<?php
        $_SESSION = array();
        session_destroy();
        // return a little feeedback message
        $this->messages[] = "You have been logged out.";
		//echo "<meta http-equiv='refresh' content='1;URL=index.php'>";
		header("Location:login.php?location=" . urlencode($_SERVER['REQUEST_URI']));
		
?>
