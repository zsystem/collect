CREATE DATABASE  IF NOT EXISTS `collect` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `collect`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: collect
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hist`
--

DROP TABLE IF EXISTS `hist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hist` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(64) NOT NULL,
  `link_id` int(11) NOT NULL,
  `field` varchar(64) NOT NULL,
  `type` varchar(12) NOT NULL,
  `old_value` varchar(64) NOT NULL,
  `new_value` varchar(64) NOT NULL,
  `maint_date` datetime DEFAULT NULL,
  `user` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hist`
--

LOCK TABLES `hist` WRITE;
/*!40000 ALTER TABLE `hist` DISABLE KEYS */;
INSERT INTO `hist` VALUES (1,'cycle',4,'CYCLE CODE','UPDATE','3','4','2014-09-12 21:37:33','admin'),(2,'cycle',4,'CYCLE CODE','UPDATE','3','4','2014-09-12 21:38:12','admin'),(3,'cycle',4,'CYCLE CODE','UPDATE','3','4','2014-09-12 21:38:44','admin'),(4,'cycle',5,'CYCLE CODE','UPDATE','aa','6','2014-09-12 21:48:51','admin'),(5,'cycle',5,'CYCLE CODE','UPDATE','6','5','2014-09-12 21:49:02','admin'),(6,'cycle',6,'CYCLE CODE','UPDATE','aa','5','2014-09-12 21:49:23','admin'),(7,'cycle',6,'CYCLE CODE','UPDATE','5','6','2014-09-12 21:50:19','admin'),(8,'cycle',7,'CYCLE CODE','UPDATE','1','7','2014-09-12 22:01:36','admin'),(9,'severity',1,'SEVERITY','UPDATE','Critical','CRITICAL','2014-09-14 02:19:45',''),(10,'severity',2,'SEVERITY','UPDATE','High','HIGH','2014-09-14 02:19:56',''),(11,'severity',3,'SEVERITY','UPDATE','Medium','MEDIUM','2014-09-14 02:20:41',''),(12,'severity',4,'SEVERITY','UPDATE','Low','LOW','2014-09-14 02:20:48',''),(13,'statmast',1,'STATUS','UPDATE','Active','ACTIVE','2014-09-14 07:19:51','admin'),(14,'statmast',2,'STATUS','UPDATE','Blocked','BLOCKED','2014-09-14 07:21:15','admin'),(15,'statmast',3,'STATUS','UPDATE','Expired','EXPIRED','2014-09-14 07:21:24','admin'),(16,'statmast',4,'STATUS','UPDATE','Renewed','RENEWED','2014-09-14 07:21:32','admin'),(17,'severity',15,'CODE TYPE','UPDATE','','C01','2014-09-14 07:57:15',''),(18,'severity',15,'CODE TYPE','UPDATE','C01','C02','2014-09-14 07:59:29',''),(19,'severity',15,'CODE TYPE','UPDATE','C02','C04','2014-09-14 08:00:04',''),(20,'severity',15,'CODE TYPE','UPDATE','C04','C02','2014-09-14 16:02:42','SYST'),(21,'limits',1,'FLOOR','UPDATE','','0.00','2014-09-14 17:25:54','SYST'),(22,'limits',1,'CEILING','UPDATE','','4999.99','2014-09-14 17:25:54','SYST'),(23,'limits',1,'FLOOR','UPDATE','','0.00','2014-09-14 17:26:22','SYST'),(24,'limits',1,'CEILING','UPDATE','','4999.99','2014-09-14 17:26:22','SYST'),(25,'limits',1,'FLOOR','UPDATE','','0.00','2014-09-14 17:28:10','SYST'),(26,'limits',1,'CEILING','UPDATE','','4999.99','2014-09-14 17:28:10','SYST'),(27,'limits',1,'FLOOR','UPDATE','','0.00','2014-09-14 17:28:24','SYST'),(28,'limits',1,'CEILING','UPDATE','','49999','2014-09-14 17:28:24','SYST'),(29,'limits',1,'FLOOR','UPDATE','','0.00','2014-09-14 17:29:30','SYST'),(30,'limits',1,'CEILING','UPDATE','','4999.99','2014-09-14 17:29:30','SYST'),(31,'limits',1,'FLOOR','UPDATE','','aa1','2014-09-14 17:39:52','SYST'),(32,'limits',1,'CEILING','UPDATE','','4999.99','2014-09-14 17:39:52','SYST'),(33,'limits',2,'FLOOR','UPDATE','','RM 5,000.00','2014-09-14 17:56:53','SYST'),(34,'limits',2,'CEILING','UPDATE','','RM 9,999.99','2014-09-14 17:56:53','SYST'),(35,'limits',2,'FLOOR','UPDATE','','RM 5,000.00','2014-09-14 17:57:51','SYST'),(36,'limits',2,'CEILING','UPDATE','','RM 0.00','2014-09-14 17:57:51','SYST'),(37,'limits',2,'FLOOR','UPDATE','','5,000.00','2014-09-14 18:10:56','SYST'),(38,'limits',2,'CEILING','UPDATE','','9,999.99','2014-09-14 18:10:56','SYST'),(39,'limits',3,'FLOOR','UPDATE','','.','2014-09-14 18:19:56','SYST'),(40,'limits',3,'CEILING','UPDATE','','.','2014-09-14 18:19:56','SYST'),(41,'limits',3,'FLOOR','UPDATE','','0','2014-09-14 18:20:40','SYST'),(42,'limits',3,'CEILING','UPDATE','','0','2014-09-14 18:20:40','SYST'),(43,'limits',5,'FLOOR','UPDATE','','10.000.00','2014-09-14 18:23:48','SYST'),(44,'limits',5,'CEILING','UPDATE','','14.999.00','2014-09-14 18:23:48','SYST'),(45,'limits',6,'FLOOR','UPDATE','','15.000.00','2014-09-14 18:23:55','SYST'),(46,'limits',6,'CEILING','UPDATE','','20.000.00','2014-09-14 18:23:55','SYST'),(47,'limits',3,'FLOOR','UPDATE','','0.00','2014-09-14 18:24:11','SYST'),(48,'limits',3,'CEILING','UPDATE','','4.999.99','2014-09-14 18:24:11','SYST'),(49,'limits',7,'CEILING','UPDATE','','34999.99','2014-09-14 19:25:07','SYST'),(50,'limits',7,'FLOOR','UPDATE','','30000.00','2014-09-14 19:25:40','SYST'),(51,'limits',7,'CEILING','UPDATE','','34999.99','2014-09-14 19:25:40','SYST'),(52,'severity',1,'SEVERITY','UPDATE','','CRITICAL','2014-09-14 19:27:18','SYST'),(53,'severity',1,'CODE TYPE','UPDATE','','C01','2014-09-14 19:27:18','SYST'),(54,'limits',11,'FLOOR','UPDATE','','50000.00','2014-09-14 19:50:42','SYST'),(55,'limits',11,'CEILING','UPDATE','','199999.99','2014-09-14 19:50:42','SYST'),(56,'cfmast',1,'OFFICE NUM','UPDATE','','0389099889','2014-09-14 21:12:14','SYST'),(57,'cfmast',1,'ZONE','UPDATE','1','2','2014-09-14 21:12:14','SYST'),(58,'cfmast',1,'LIMIT','UPDATE','13','20','2014-09-14 21:14:47','SYST'),(59,'severity',7,'CODE TYPE','UPDATE','','C04','2014-09-14 21:49:00','SYST'),(60,'severity',12,'CODE TYPE','UPDATE','','C03','2014-09-14 21:50:16','SYST'),(61,'cfmast',1,'PHOTO','UPDATE','OLD PHOTO','Koala.jpg','2014-09-14 14:06:14',''),(62,'cfmast',1,'PHOTO','UPDATE','OLD PHOTO','','2014-09-14 14:08:43','SYST'),(63,'cfmast',1,'PHOTO','UPDATE','OLD PHOTO','','2014-09-14 14:10:34','SYST'),(64,'cfmast',1,'PHOTO','UPDATE','OLD PHOTO','Desert.jpg','2014-09-14 14:11:37','SYST'),(65,'cfmast',1,'PHOTO','UPDATE','OLD PHOTO','Koala.jpg','2014-09-15 08:44:12','SYST'),(66,'propmast',1,'ZONE','UPDATE','1','2','2014-09-16 23:02:53','SP1'),(67,'cfmast',1,'PHOTO','UPDATE','OLD PHOTO','Collect1.jpg','2014-09-16 15:15:15','SP1'),(68,'cfmast',3,'OFFICE NUM','UPDATE','','0313123131','2014-09-16 23:43:03','SP1'),(69,'cfmast',3,'ZONE','UPDATE','1','2','2014-09-16 23:43:03','SP1'),(70,'cfmast',3,'ZONE','UPDATE','2','1','2014-09-16 23:43:17','SP1'),(71,'propmast',1,'ZONE','UPDATE','2','1','2014-09-16 23:54:28','SP1'),(72,'cfmast',2,'ID NUM','UPDATE','A09908883','','2014-09-17 00:01:51','SP1'),(73,'cfmast',2,'ID NUM','UPDATE','','00900112','2014-09-17 00:02:09','SP1'),(74,'cfmast',2,'SALUTATION','UPDATE','MRS','MR','2014-09-17 00:02:39','SP1'),(75,'cycle',9,'TYPE','UPDATE','','Period','2014-09-28 23:24:00','SYST'),(76,'cfmast',4,'E-MAIL','UPDATE','','eh.ban@yahoo.com','2014-09-29 20:55:30','SYST'),(77,'cfmast',4,'ID TYPE','UPDATE','','','2014-09-29 20:55:30','SYST'),(78,'cfmast',4,'ID NUM','UPDATE','','A19980075','2014-09-29 20:55:30','SYST'),(79,'cfmast',4,'MOBILE NUM','UPDATE','','0189989983','2014-09-29 20:55:30','SYST'),(80,'cfmast',4,'LOT NO','UPDATE','','No 14','2014-09-29 20:55:30','SYST'),(81,'cfmast',4,'ADDRESS 1','UPDATE','','Jalan Kesuma','2014-09-29 20:55:30','SYST'),(82,'cfmast',4,'ADDRESS 2','UPDATE','','Bandar Tasik Kesuma','2014-09-29 20:55:30','SYST'),(83,'cfmast',4,'POSTCODE','UPDATE','','43500','2014-09-29 20:55:30','SYST'),(84,'cfmast',4,'AREA','UPDATE','','Bandar Teknologi Kajang','2014-09-29 20:55:30','SYST'),(85,'cfmast',4,'LIMIT','UPDATE','1','19','2014-09-29 20:55:30','SYST'),(86,'cfmast',4,'PHOTO','UPDATE','OLD PHOTO','Koala.jpg','2014-09-29 12:55:56','SYST'),(87,'cfmast',3,'LIMIT','UPDATE','1','17','2014-09-29 23:33:45','Officer1'),(88,'feedback',5,'STATUS','UPDATE','Reminder sent','BROKEN PROMISE','2014-10-11 05:28:49','SYST'),(89,'feedback',5,'SEVERITY','UPDATE','6','17','2014-10-11 05:28:49','SYST'),(90,'feedback',1,'SEVERITY','UPDATE','6','17','2014-10-11 05:29:00','SYST'),(91,'severity',7,'SEVERITY','UPDATE','WARNING','ALERT','2014-10-11 13:30:34','SYST'),(92,'feedback',2,'SEVERITY','UPDATE','1','7','2014-10-11 05:31:06','SYST');
/*!40000 ALTER TABLE `hist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-12 18:37:10
